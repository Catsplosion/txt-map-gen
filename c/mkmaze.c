/* Matt: The bughack variable used in wall_cleanup is created as this.
Apparently somewhting to do with insect legs, so i would say for now it can be ignored.
But a lev region looks like below.*/

/* for preserving the insect legs when wallifying baalz level */
static lev_region bughack = { {COLNO, ROWNO, 0, 0}, {COLNO, ROWNO, 0, 0} };

/* Matt: the lev_region from sp_lev.h:*/

/* values for rtype are defined in dungeon.h */
typedef struct {
	struct {
		xchar x1, y1, x2, y2;
	} inarea;
	struct {
		xchar x1, y1, x2, y2;
	} delarea;
	boolean in_islev, del_islev;
	xchar rtype, padding;
	Str_or_Len rname;
} lev_region;


/* Matt: The macro within_bounded_aread from dungeon.h for reference in wall_cleanup
Seems to just check that X and Y are within the boxed area defined by L and H x and y,
meaning low and high.*/
#define within_bounded_area(X, Y, LX, LY, HX, HY) \
    ((X) >= (LX) && (X) <= (HX) && (Y) >= (LY) && (Y) <= (HY))



/* Matt: Taken from mkmaze.c to reference in wall_cleanup.
The breakdown of isok() can be found in create_maze_func.c, but it just checks if the position
is within the terminal dimensions.
IS_STWALL is just a macro that checks that it is less than DBWALL in the enum, for solid wall.*/

/* return TRUE if out of bounds, wall or rock */
STATIC_OVL boolean
is_solid(x, y)
int x, y;
{
	return (boolean)(!isok(x, y) || IS_STWALL(levl[x][y].typ));
}


/* Matt: Taken from mkmaze.c for reference in wallification below.*/

/* Remove walls totally surrounded by stone */
void
wall_cleanup(x1, y1, x2, y2)
int x1, y1, x2, y2;
{
	uchar type;
	int x, y;
	struct rm *lev; // Matt: An rm is the struct holding info on a tile within the game.

	// Makes sense.
	/* sanity check on incoming variables */
	if (x1 < 0 || x2 >= COLNO || x1 > x2 || y1 < 0 || y2 >= ROWNO || y1 > y2)
		panic("wall_cleanup: bad bounds (%d,%d) to (%d,%d)", x1, y1, x2, y2);

	// Matt:L Seems to be finding walls that are stand alone, ie non next to some corridor 
	// or room Then blending them back in with the rock.

	/* change walls surrounded by rock to rock. */
	for (x = x1; x <= x2; x++) // Go through all x's
		for (y = y1; y <= y2; y++) { // and y's

			// Check if this x y is within the bounding area of bughack.inarea.
			// Which seems to be terminal size to 0??? See above. I feel like
			// this can probably be ignored as having something to do with baalz level.
			if (within_bounded_area(x, y,
				bughack.inarea.x1, bughack.inarea.y1,
				bughack.inarea.x2, bughack.inarea.y2))
				continue;
			lev = &levl[x][y]; // Get the current rm object at this xy.
			type = lev->typ; // Get its type.
			if (IS_WALL(type) && type != DBWALL) { // If this is a wall, and not a DBWALL.
				// If the spaces in the four cardinal dirs around us are solid.
				if (is_solid(x - 1, y - 1) && is_solid(x - 1, y)
					&& is_solid(x - 1, y + 1) && is_solid(x, y - 1)
					&& is_solid(x, y + 1) && is_solid(x + 1, y - 1)
					&& is_solid(x + 1, y) && is_solid(x + 1, y + 1))
					lev->typ = STONE; // We are a single wall alone so set to STONE.
			}
		}
}


/* Matt: taken from mkmaze.c. For reference in fix_wall_spines below.*/

/*
 * Return 1 (not TRUE - we're doing bit vectors here) if we want to extend
 * a wall spine in the (dx,dy) direction.  Return 0 otherwise.
 *
 * To extend a wall spine in that direction, first there must be a wall there.
 * Then, extend a spine unless the current position is surrounded by walls
 * in the direction given by (dx,dy).  E.g. if 'x' is our location, 'W'
 * a wall, '.' a room, 'a' anything (we don't care), and our direction is
 * (0,1) - South or down - then:
 *
 *              a a a
 *              W x W           This would not extend a spine from x down
 *              W W W           (a corridor of walls is formed).
 *
 *              a a a
 *              W x W           This would extend a spine from x down.
 *              . W W
 */
STATIC_OVL int
extend_spine(locale, wall_there, dx, dy)
int locale[3][3];
int wall_there, dx, dy;
{
    int spine, nx, ny;

    nx = 1 + dx;
    ny = 1 + dy;

    if (wall_there) { /* wall in that direction */
        if (dx) {
            if (locale[1][0] && locale[1][2]         /* EW are wall/stone */
                && locale[nx][0] && locale[nx][2]) { /* diag are wall/stone */
                spine = 0;
            } else {
                spine = 1;
            }
        } else { /* dy */
            if (locale[0][1] && locale[2][1]         /* NS are wall/stone */
                && locale[0][ny] && locale[2][ny]) { /* diag are wall/stone */
                spine = 0;
            } else {
                spine = 1;
            }
        }
    } else {
        spine = 0;
    }

    return spine;
}



/* Matt: Taken from mkmaze.c for reference in wallification below.*/

/* Correct wall types so they extend and connect to each other */
void
fix_wall_spines(x1, y1, x2, y2)
int x1, y1, x2, y2;
{
	uchar type;
	int x, y;
	struct rm *lev; // rm is the struct of a tile in the game.
	int FDECL((*loc_f), (int, int));
	int bits;
	int locale[3][3]; /* rock or wall status surrounding positions */

	/*
	 * Value 0 represents a free-standing wall.  It could be anything,
	 * so even though this table says VWALL, we actually leave whatever
	 * typ was there alone.
	 */
	static xchar spine_array[16] = { VWALL, HWALL,    HWALL,    HWALL,
									 VWALL, TRCORNER, TLCORNER, TDWALL,
									 VWALL, BRCORNER, BLCORNER, TUWALL,
									 VWALL, TLWALL,   TRWALL,   CROSSWALL };

	/* sanity check on incoming variables */
	if (x1 < 0 || x2 >= COLNO || x1 > x2 || y1 < 0 || y2 >= ROWNO || y1 > y2)
		panic("wall_extends: bad bounds (%d,%d) to (%d,%d)", x1, y1, x2, y2);

	/* set the correct wall type. */
	for (x = x1; x <= x2; x++)
		for (y = y1; y <= y2; y++) {
			lev = &levl[x][y];
			type = lev->typ;
			if (!(IS_WALL(type) && type != DBWALL))
				continue;

			/* set the locations TRUE if rock or wall or out of bounds */
			loc_f = within_bounded_area(x, y, /* for baalz insect */
				bughack.inarea.x1, bughack.inarea.y1,
				bughack.inarea.x2, bughack.inarea.y2)
				? iswall
				: iswall_or_stone;
			locale[0][0] = (*loc_f)(x - 1, y - 1);
			locale[1][0] = (*loc_f)(x, y - 1);
			locale[2][0] = (*loc_f)(x + 1, y - 1);

			locale[0][1] = (*loc_f)(x - 1, y);
			locale[2][1] = (*loc_f)(x + 1, y);

			locale[0][2] = (*loc_f)(x - 1, y + 1);
			locale[1][2] = (*loc_f)(x, y + 1);
			locale[2][2] = (*loc_f)(x + 1, y + 1);

			/* determine if wall should extend to each direction NSEW */
			bits = (extend_spine(locale, iswall(x, y - 1), 0, -1) << 3)
				| (extend_spine(locale, iswall(x, y + 1), 0, 1) << 2)
				| (extend_spine(locale, iswall(x + 1, y), 1, 0) << 1)
				| extend_spine(locale, iswall(x - 1, y), -1, 0);

			/* don't change typ if wall is free-standing */
			if (bits)
				lev->typ = spine_array[bits];
		}
}


/* Matt: Taken from mkmaze.c fro reference to wallification in makemaz below.
Just calls wall_cleanup and fix_wall_spines*/

void
wallification(x1, y1, x2, y2)
int x1, y1, x2, y2;
{
	wall_cleanup(x1, y1, x2, y2);
	fix_wall_spines(x1, y1, x2, y2);
}



/* Matt: Taken from mkmaze.c for reference in makemaz below.
Fairly straight forward.*/

/* find random point in generated corridors,
   so we don't create items in moats, bunkers, or walls */
void
mazexy(cc)
coord *cc;
{
	int cpt = 0;

	do {
		cc->x = 1 + rn2(x_maze_max);
		cc->y = 1 + rn2(y_maze_max);
		cpt++;
	} while (cpt < 100
		&& levl[cc->x][cc->y].typ
		!= (level.flags.corrmaze ? CORR : ROOM));
	if (cpt >= 100) {
		int x, y;

		/* last try */
		for (x = 1; x < x_maze_max; x++)
			for (y = 1; y < y_maze_max; y++) {
				cc->x = x;
				cc->y = y;
				if (levl[cc->x][cc->y].typ
					== (level.flags.corrmaze ? CORR : ROOM))
					return;
			}
		panic("mazexy: can't find a place!");
	}
	return;
}


/* Matt: The mkstairs function from mklev.c for reference in makemaz below where it makes the stairs.
Pretty self explanatory, just unsure about the mkroom. Because that seems to be null when passed here.
Is it just a marker to make a room here when the player goes in to this level, seems to be.*/

void
mkstairs(x, y, up, croom)
xchar x, y;
char up;
struct mkroom *croom;
{
	if (!x) {
		impossible("mkstairs:  bogus stair attempt at <%d,%d>", x, y);
		return;
	}

	/*
	 * We can't make a regular stair off an end of the dungeon.  This
	 * attempt can happen when a special level is placed at an end and
	 * has an up or down stair specified in its description file.
	 */
	if ((dunlev(&u.uz) == 1 && up)
		|| (dunlev(&u.uz) == dunlevs_in_dungeon(&u.uz) && !up))
		return;

	if (up) {
		xupstair = x;
		yupstair = y;
		upstairs_room = croom;
	}
	else {
		xdnstair = x;
		ydnstair = y;
		dnstairs_room = croom;
	}

	levl[x][y].typ = STAIRS;
	levl[x][y].ladder = up ? LA_UP : LA_DOWN;
}


void
makemaz(s)
const char *s;
{
	// Vars
	int x, y;
	char protofile[20]; // The prototype layout file
	s_level *sp = Is_special(&u.uz); // Null if not special level.
	coord mm;

	/* Matt: All of this for getting the prototype file when we are given a name in s, so not maze related.
	##############################################################*/
	if (*s) {
		if (sp && sp->rndlevs)
			Sprintf(protofile, "%s-%d", s, rnd((int)sp->rndlevs));
		else
			Strcpy(protofile, s);
	}
	else if (*(dungeons[u.uz.dnum].proto)) {
		if (dunlevs_in_dungeon(&u.uz) > 1) {
			if (sp && sp->rndlevs)
				Sprintf(protofile, "%s%d-%d", dungeons[u.uz.dnum].proto,
					dunlev(&u.uz), rnd((int)sp->rndlevs));
			else
				Sprintf(protofile, "%s%d", dungeons[u.uz.dnum].proto,
					dunlev(&u.uz));
		}
		else if (sp && sp->rndlevs) {
			Sprintf(protofile, "%s-%d", dungeons[u.uz.dnum].proto,
				rnd((int)sp->rndlevs));
		}
		else
			Strcpy(protofile, dungeons[u.uz.dnum].proto);
		// #######################################################
	}
	else
		// Here is where we get to make maze level because we dont have protofile.
		Strcpy(protofile, "");

	/* Matt: This seems to be related to special level file stuff when the user is in
	wizard mode. ############################################################*/

	/* SPLEVTYPE format is "level-choice,level-choice"... */
	if (wizard && *protofile && sp && sp->rndlevs) {
		char *ep = getenv("SPLEVTYPE"); /* not nh_getenv */
		if (ep) {
			/* rindex always succeeds due to code in prior block */
			int len = (int)((rindex(protofile, '-') - protofile) + 1);

			while (ep && *ep) {
				if (!strncmp(ep, protofile, len)) {
					int pick = atoi(ep + len);
					/* use choice only if valid */
					if (pick > 0 && pick <= (int)sp->rndlevs)
						Sprintf(protofile + len, "%d", pick);
					break;
				}
				else {
					ep = index(ep, ',');
					if (ep)
						++ep;
				}
			}
		}
	}
	// ######################################################################


	// Matt: Literally no idea ##########################
	if (*protofile) {
		check_ransacked(protofile);
		Strcat(protofile, LEV_EXT);
		if (load_special(protofile)) {
			/* some levels can end up with monsters
			   on dead mon list, including light source monsters */
			dmonsfree();
			return; /* no mazification right now */
		}
		impossible("Couldn't load \"%s\" - making a maze.", protofile);
	}
	// ##################################################

	 // Matt: Look at this???
	level.flags.is_maze_lev = TRUE;
	level.flags.corrmaze = !rn2(3); // This affects whether we make the maze out of rooms or corridors.
	// No idea what affect this really has on gameplay but affects gen a little later on.

	if (!Invocation_lev(&u.uz) && rn2(2)) {
		int corrscale = rnd(4);
		create_maze(corrscale, rnd(4) - corrscale); // Creating a larger corridor maze.
	}
	else {
		create_maze(1, 1); // Creating 1 wall, 1 corridor thick maze.
	}

	if (!level.flags.corrmaze) // If we are not doing CORR for maze, but ROOM instead.
		wallification(2, 2, x_maze_max, y_maze_max);

	mazexy(&mm); // Get random location within maze.
	mkstairs(mm.x, mm.y, 1, (struct mkroom *) 0); /* up */
	if (!Invocation_lev(&u.uz)) { // Not in hell and at the bottom of the dungeon?
		mazexy(&mm);
		mkstairs(mm.x, mm.y, 0, (struct mkroom *) 0); /* down */
	}

	/* Matt: All of this seems to be relating to something called Molochs sanctum, So,
	for now I think dont worry about this.*/

	else { /* choose "vibrating square" location */
#define x_maze_min 2
#define y_maze_min 2
/*
 * Pick a position where the stairs down to Moloch's Sanctum
 * level will ultimately be created.  At that time, an area
 * will be altered:  walls removed, moat and traps generated,
 * boulders destroyed.  The position picked here must ensure
 * that that invocation area won't extend off the map.
 *
 * We actually allow up to 2 squares around the usual edge of
 * the area to get truncated; see mkinvokearea(mklev.c).
 */
#define INVPOS_X_MARGIN (6 - 2)
#define INVPOS_Y_MARGIN (5 - 2)
#define INVPOS_DISTANCE 11
		int x_range = x_maze_max - x_maze_min - 2 * INVPOS_X_MARGIN - 1,
			y_range = y_maze_max - y_maze_min - 2 * INVPOS_Y_MARGIN - 1;

		if (x_range <= INVPOS_X_MARGIN || y_range <= INVPOS_Y_MARGIN
			|| (x_range * y_range) <= (INVPOS_DISTANCE * INVPOS_DISTANCE)) {
			debugpline2("inv_pos: maze is too small! (%d x %d)",
				x_maze_max, y_maze_max);
		}
		inv_pos.x = inv_pos.y = 0; /*{occupied() => invocation_pos()}*/
		do {
			x = rn1(x_range, x_maze_min + INVPOS_X_MARGIN + 1);
			y = rn1(y_range, y_maze_min + INVPOS_Y_MARGIN + 1);
			/* we don't want it to be too near the stairs, nor
			   to be on a spot that's already in use (wall|trap) */
		} while (x == xupstair || y == yupstair /*(direct line)*/
			|| abs(x - xupstair) == abs(y - yupstair)
			|| distmin(x, y, xupstair, yupstair) <= INVPOS_DISTANCE
			|| !SPACE_POS(levl[x][y].typ) || occupied(x, y));
		inv_pos.x = x;
		inv_pos.y = y;
		maketrap(inv_pos.x, inv_pos.y, VIBRATING_SQUARE);
#undef INVPOS_X_MARGIN
#undef INVPOS_Y_MARGIN
#undef INVPOS_DISTANCE
#undef x_maze_min
#undef y_maze_min
	}

	/* place branch stair or portal */
	place_branch(Is_branchlev(&u.uz), 0, 0);

	// Place all the objects/monsters and stuff.
	for (x = rn1(8, 11); x; x--) {
		mazexy(&mm);
		(void)mkobj_at(rn2(2) ? GEM_CLASS : 0, mm.x, mm.y, TRUE);
	}
	for (x = rn1(10, 2); x; x--) {
		mazexy(&mm);
		(void)mksobj_at(BOULDER, mm.x, mm.y, TRUE, FALSE);
	}
	for (x = rn2(3); x; x--) {
		mazexy(&mm);
		(void)makemon(&mons[PM_MINOTAUR], mm.x, mm.y, NO_MM_FLAGS);
	}
	for (x = rn1(5, 7); x; x--) {
		mazexy(&mm);
		(void)makemon((struct permonst *) 0, mm.x, mm.y, NO_MM_FLAGS);
	}
	for (x = rn1(6, 7); x; x--) {
		mazexy(&mm);
		(void)mkgold(0L, mm.x, mm.y);
	}
	for (x = rn1(6, 7); x; x--)
		mktrap(0, 1, (struct mkroom *) 0, (coord *)0);
}