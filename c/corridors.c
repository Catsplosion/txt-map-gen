/* Matt: This is the comparison function that is passed into quick sort
for the sorting of the rooms within the level.

So for our implementation, this is essentially sorting the 2 rooms passed to
it by there low x value. So from left to right on the map.
*/

STATIC_PTR int CFDECLSPEC
do_comp(vx, vy)
const genericptr vx;
const genericptr vy;
{
#ifdef LINT
	/* lint complains about possible pointer alignment problems, but we know
	   that vx and vy are always properly aligned. Hence, the following
	   bogus definition:
	*/
	return (vx == vy) ? 0 : -1;
#else
	register const struct mkroom *x, *y;

	x = (const struct mkroom *) vx;
	y = (const struct mkroom *) vy;
	if (x->lx < y->lx)
		return -1;
	return (x->lx > y->lx);
#endif /* LINT */
}

/* Matt: This is the sort_rooms function that actually calls the
system provided qsort*/

void
sort_rooms()
{
#if defined(SYSV) || defined(DGUX)
	qsort((genericptr_t)rooms, (unsigned)nroom, sizeof(struct mkroom),
		do_comp);
#else
	qsort((genericptr_t)rooms, nroom, sizeof(struct mkroom), do_comp);
#endif
}


/*Matt: This is the function to generate the corridors between the
sorted collection of rooms.*/

void
makecorridors()
{
	int a, b, i;
	boolean any = TRUE;

	// Matt: Go through the array joining each room to the one next to
	// it in the array, so the next one to the right.
	for (a = 0; a < nroom - 1; a++) {
		join(a, a + 1, FALSE);
		if (!rn2(50)) // Matt: or not
			break; /* allow some randomness */
	}
	// Matt: Go through again
	for (a = 0; a < nroom - 2; a++)
		// So when rooms are created the number room they are is
		// added to smeq in that index position. So if smeq[a] !=
		// smeq[a + 2] that means that the rooms are not the same room.
		// Then we are joining each room to the one 2 steps down the array.
		if (smeq[a] != smeq[a + 2])
			join(a, a + 2, FALSE);

	// Matt: So here we are going through again and trying to join
	// every room to every room.
	for (a = 0; any && a < nroom; a++) {
		any = FALSE; // This will cause the for loop to end.
		// And now we go through the array again, within the elements from
		// the above loop.
		for (b = 0; b < nroom; b++) 
			// So once again, this checks if these rooms are not
			// equal.
			if (smeq[a] != smeq[b]) {
				// If they arent the same room, join them.
				join(a, b, FALSE);
				// Set any to TRUE, saying we are still dealing with some
				// rooms within these loops that aren't the same, and
				// so the outer loop should continue.
				any = TRUE;
			}
	}
	// Matt: If we are dealing with more than 2 rooms.
	if (nroom > 2)
		// Count down from some random number plus 4? why 4?
		for (i = rn2(nroom) + 4; i; i--) {
			a = rn2(nroom); // Random room a.
			// Random room b.
			// We minus 2 from the nroom to get the random
			// number, because below we may add 2 to the number.
			// stopping overflow.
			b = rn2(nroom - 2);
			// If the b index is greater than or equar to the a index,
			// then add 2. So this stops us from trying to join the same
			// room randombly, and also from entering the rooms into
			// join in the wrong order. I.e the room furthest to the left
			// should be argument 1. Not actually sure if arg order matters?
			// By the looks of join it doesn't.
			if (b >= a)
				b += 2;
			join(a, b, TRUE);
		}
}


/* Matt: This is the join function, that actually maps the corridor between
2 rooms it has selected.*/



STATIC_OVL void
join(a, b, nxcor)
register int a, b;
boolean nxcor;
{
	coord cc, tt, org, dest;
	register xchar tx, ty, xx, yy;
	register struct mkroom *croom, *troom;
	register int dx, dy;

	croom = &rooms[a]; //Matt: From room
	troom = &rooms[b]; //Matt: to room

	/* find positions cc and tt for doors in croom and troom
	   and direction for a corridor between them */

	// Matt: Cant think of any case in our example where the high,
	// x of a room could be less than 0? Also will we want a doormax?
	if (troom->hx < 0 || croom->hx < 0 || doorindex >= DOORMAX)
		return;

	// The process below is repeated for each different direction,
	// in its case.
	// Here troom is on the right and croom the left.
	if (troom->lx > croom->hx) {
		// The dx dy values denote which direction we are moving in
		// on specified axis.
		dx = 1;
		dy = 0;
		// So on the right hand size of croom, as its to the left.
		xx = croom->hx + 1;
		// On the left size of troom, as its on the right.
		tx = troom->lx - 1;
		// Scan the line of x positions to the left and right of the rooms,
		// acroos the y range looking for a good door pos.
		finddpos(&cc, xx, croom->ly, xx, croom->hy);
		finddpos(&tt, tx, troom->ly, tx, troom->hy);
	}

	// Here troom is above croom.
	else if (troom->hy < croom->ly) {
		dy = -1;
		dx = 0;
		yy = croom->ly - 1;
		finddpos(&cc, croom->lx, yy, croom->hx, yy);
		ty = troom->hy + 1;
		finddpos(&tt, troom->lx, ty, troom->hx, ty);
	}
	// Here troom is to the left of croom.
	else if (troom->hx < croom->lx) {
		dx = -1;
		dy = 0;
		xx = croom->lx - 1;
		tx = troom->hx + 1;
		finddpos(&cc, xx, croom->ly, xx, croom->hy);
		finddpos(&tt, tx, troom->ly, tx, troom->hy);
	}
	// here troom is below croom.
	else {
		dy = 1;
		dx = 0;
		yy = croom->hy + 1;
		ty = troom->ly - 1;
		finddpos(&cc, croom->lx, yy, croom->hx, yy);
		finddpos(&tt, troom->lx, ty, troom->hx, ty);
	}

	// Matt: At this point tt will have a position in it for the door
	// at the troom side, and cc will have the same for the croom side.

	xx = cc.x;
	yy = cc.y;
	// MAtt: Here if for example the troom was to the right of the croom.
	// then tx = the x pos of the door - 1, because troom is on the right
	// we need to take x values away to go left towards croom.
	tx = tt.x - dx;
	ty = tt.y - dy;
	// MAtt:nxcor only seems to be set when we are doing random corridors?
	// So if nxcor is true, and the position that is in the direction
	// we are traveling next to the croom door is anythin other than
	// stone, then we just exit? Seems to be, when placing randoms
	// if you hit something in the way just give up?
	if (nxcor && levl[xx + dx][yy + dy].typ)
		return;
	// Matt: If the croom door pos is okay for a door, or we are not
	// doing nxcor place the door.
	// So it seems if nxcor is false, it doesn't even matter if the pos
	// is not good for a door, we just slam it down anyway?
	// But didnt finddpos() check?
	if (okdoor(xx, yy) || !nxcor)
		dodoor(xx, yy, croom);

	// Matt: Origin x and y are the croom x and y + the direction we are going.
	org.x = xx + dx;
	org.y = yy + dy;
	// Matt: Destination is just the troom x and y.
	dest.x = tx;
	dest.y = ty;

	// Matt: Lets Dig.
	if (!dig_corridor(&org, &dest, nxcor, level.flags.arboreal ? ROOM : CORR,
		STONE))
		return;

	/* we succeeded in digging the corridor */
	if (okdoor(tt.x, tt.y) || !nxcor)
		dodoor(tt.x, tt.y, troom);

	// Matt: Why on earth do we switch the smeq values here??
	// This will have some effect in makecorridors() where join is called.
	// Because join checks smeq before running a tunnel. So maybe its checking that
	// the rooms haven't already been joined?

	// Yes, so above it checks if the smeq values are the same. So here, when we successfully
	// join two rooms, we make there smeq values equal, so they will not be joined again.
	if (smeq[a] < smeq[b])
		smeq[b] = smeq[a];
	else
		smeq[a] = smeq[b];
}


/* Matt: The dodoor function as called above in join.*/

void
dodoor(x, y, aroom)
int x, y;
struct mkroom *aroom;
{
	if (doorindex >= DOORMAX) {
		impossible("DOORMAX exceeded?");
		return;
	}

	dosdoor(x, y, aroom, rn2(8) ? DOOR : SDOOR);
}


/* Matt: This is the dosdoor function called above in dodoor

It literally seems to just control the type of door based on random chances.
We arent dealing with different door types for now so i think we can
ignore it.
*/

STATIC_OVL void
dosdoor(x, y, aroom, type)
register xchar x, y;
struct mkroom *aroom;
int type;
{
	boolean shdoor = *in_rooms(x, y, SHOPBASE) ? TRUE : FALSE;

	if (!IS_WALL(levl[x][y].typ)) /* avoid SDOORs on already made doors */
		type = DOOR;
	levl[x][y].typ = type;
	if (type == DOOR) {
		if (!rn2(3)) { /* is it a locked door, closed, or a doorway? */
			if (!rn2(5))
				levl[x][y].doormask = D_ISOPEN;
			else if (!rn2(6))
				levl[x][y].doormask = D_LOCKED;
			else
				levl[x][y].doormask = D_CLOSED;

			if (levl[x][y].doormask != D_ISOPEN && !shdoor
				&& level_difficulty() >= 5 && !rn2(25))
				levl[x][y].doormask |= D_TRAPPED;
		}
		else {
#ifdef STUPID
			if (shdoor)
				levl[x][y].doormask = D_ISOPEN;
			else
				levl[x][y].doormask = D_NODOOR;
#else
			levl[x][y].doormask = (shdoor ? D_ISOPEN : D_NODOOR);
#endif
		}

		/* also done in roguecorr(); doing it here first prevents
		   making mimics in place of trapped doors on rogue level */
		if (Is_rogue_level(&u.uz))
			levl[x][y].doormask = D_NODOOR;

		if (levl[x][y].doormask & D_TRAPPED) {
			struct monst *mtmp;

			if (level_difficulty() >= 9 && !rn2(5)
				&& !((mvitals[PM_SMALL_MIMIC].mvflags & G_GONE)
					&& (mvitals[PM_LARGE_MIMIC].mvflags & G_GONE)
					&& (mvitals[PM_GIANT_MIMIC].mvflags & G_GONE))) {
				/* make a mimic instead */
				levl[x][y].doormask = D_NODOOR;
				mtmp = makemon(mkclass(S_MIMIC, 0), x, y, NO_MM_FLAGS);
				if (mtmp)
					set_mimic_sym(mtmp);
			}
		}
		/* newsym(x,y); */
	}
	else { /* SDOOR */
		if (shdoor || !rn2(5))
			levl[x][y].doormask = D_LOCKED;
		else
			levl[x][y].doormask = D_CLOSED;

		if (!shdoor && level_difficulty() >= 4 && !rn2(20))
			levl[x][y].doormask |= D_TRAPPED;
	}

	add_door(x, y, aroom);
}


/* Matt: The add_door function as called abover in dosdoor.

Seems to literally just place the door into an array.
*/

void
add_door(x, y, aroom)
register int x, y;
register struct mkroom *aroom;
{
	register struct mkroom *broom;
	register int tmp;
	int i;

	if (aroom->doorct == 0)
		aroom->fdoor = doorindex;

	aroom->doorct++;

	for (tmp = doorindex; tmp > aroom->fdoor; tmp--)
		doors[tmp] = doors[tmp - 1];

	for (i = 0; i < nroom; i++) {
		broom = &rooms[i];
		if (broom != aroom && broom->doorct && broom->fdoor >= aroom->fdoor)
			broom->fdoor++;
	}
	for (i = 0; i < nsubroom; i++) {
		broom = &subrooms[i];
		if (broom != aroom && broom->doorct && broom->fdoor >= aroom->fdoor)
			broom->fdoor++;
	}

	doorindex++;
	doors[aroom->fdoor].x = x;
	doors[aroom->fdoor].y = y;
}




/* Matt: The define for rn1 as used below in finddpos.
Basically just a random call plus some number*/
#define rn1(x, y) (rn2(x) + (y))


/*Matt: The isok function as called in bydoor below.

This just checks that the position is within the games consoles x/y
bounds
*/

isok(x, y)
register int x, y;
{
	/* x corresponds to curx, so x==1 is the first column. Ach. %% */
	return x >= 1 && x <= COLNO - 1 && y >= 0 && y <= ROWNO - 1;
}



/*Matt: The bydoor function as called in okdoor below

This just checks in all 4 directions around the supplied point.
If the tile in that position is a door or secret door --> true
else --> false.
*/

STATIC_OVL boolean
bydoor(x, y)
register xchar x, y;
{
	register int typ;

	// Matt: So here with the first set of if's we are checking if
	// the 4 cardinal directions around out point are within bounds.
	// Then we get the type of the tile in that direction around the point.
	// Then in the nester if's if that type is a door or sectret door,
	// we return true, otherwise false.
	if (isok(x + 1, y)) {
		typ = levl[x + 1][y].typ;
		if (IS_DOOR(typ) || typ == SDOOR)
			return TRUE;
	}
	if (isok(x - 1, y)) {
		typ = levl[x - 1][y].typ;
		if (IS_DOOR(typ) || typ == SDOOR)
			return TRUE;
	}
	if (isok(x, y + 1)) {
		typ = levl[x][y + 1].typ;
		if (IS_DOOR(typ) || typ == SDOOR)
			return TRUE;
	}
	if (isok(x, y - 1)) {
		typ = levl[x][y - 1].typ;
		if (IS_DOOR(typ) || typ == SDOOR)
			return TRUE;
	}
	return FALSE;
}



/* Matt: The okdoor function as called below in finddpos.*/

/* see whether it is allowable to create a door at [x,y] */
int
okdoor(x, y)
xchar x, y;
{
	// True if we have a door next to this point.
	boolean near_door = bydoor(x, y);

	// So, you are okay to place a door if:
	// This position is currently a horizontal or vertical wall.
	// You haven't exceeded max foor count.
	// You are not right next to another door.
	return ((levl[x][y].typ == HWALL || levl[x][y].typ == VWALL)
		&& doorindex < DOORMAX && !near_door);
}



/* Matt: Here is the finddpos function for reference in its
calls in join()*/

/* MATT: For commentary on the use of this function above look
at ref find_door_pos in notebook*/

STATIC_OVL void
finddpos(cc, xl, yl, xh, yh)
coord *cc;
xchar xl, yl, xh, yh;
{
	register xchar x, y;

	// Matt: So x is a random number from 0, to high x, minus low x plus 1.
	// then we add low x to that value, same for the y's.
	x = rn1(xh - xl + 1, xl);
	y = rn1(yh - yl + 1, yl);
	// Matt: If you are okay to place a door, go to gotit.
	if (okdoor(x, y))
		goto gotit;

	// Here we could not place a door in the previous x/y.

	// For all the x's between low x and high x
	for (x = xl; x <= xh; x++)
		// For all the y's
		for (y = yl; y <= yh; y++)
			// Can we put a door, if so this is good!
			if (okdoor(x, y))
				goto gotit;


	for (x = xl; x <= xh; x++)
		for (y = yl; y <= yh; y++)
			// We couldnt find a position for the door that was empty here
			// So instead we look for a position that is already a door
			// or secret door, and just give that position back.
			// recycling!!
			if (IS_DOOR(levl[x][y].typ) || levl[x][y].typ == SDOOR)
				goto gotit;


	// Matt: If we couldnt find one then you will just get
	// the low x and high y back.

	/* cannot find something reasonable -- strange */
	x = xl;
	y = yh;
gotit:
	// We got a position for the door, so into our output reference,
	// put the current x and y.
	cc->x = x;
	cc->y = y;
	return;
}