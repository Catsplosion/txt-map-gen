
/* Matt: This is the macro for a bit field taken from global.h for reference.
It allows the creatioin of bit fields of some name in a structure. With the c syntax
where : can be used to set the number of bits that will be used for this variable.
This essentially causes the compiler to pack.*/

#define Bitfield(x, n) unsigned x : n


/* Matt: This was taken from rm.h for reference. It seems to be the structure that essentially
stores all the information for a tile or point in the game*/

/*
 * The structure describing a coordinate position.
 * Before adding fields, remember that this will significantly affect
 * the size of temporary files and save files.
 *
 * Also remember that the run-length encoding for some ports in save.c
 * must be updated to consider the field.
 */
struct rm {
	int glyph;               /* what the hero thinks is there */
	schar typ;               /* what is really there */
	uchar seenv;             /* seen vector */
	Bitfield(flags, 5);      /* extra information for typ */
	Bitfield(horizontal, 1); /* wall/door/etc is horiz. (more typ info) */
	Bitfield(lit, 1);        /* speed hack for lit rooms */
	Bitfield(waslit, 1);     /* remember if a location was lit */

	Bitfield(roomno, 6); /* room # for special rooms */
	Bitfield(edge, 1);   /* marks boundaries for special rooms*/
	Bitfield(candig, 1); /* Exception to Can_dig_down; was a trapdoor */
};



/* Matt: Taken from rm.h for reference. This is the structure for a level
which contrains the array of tiles as above ^. The tiles array is referenced directly
by levl via macro below.*/

typedef struct {
	struct rm locations[COLNO][ROWNO]; // This is the locations represented by levl in the alg.
#ifndef MICROPORT_BUG
	struct obj *objects[COLNO][ROWNO];
	struct monst *monsters[COLNO][ROWNO];
#else
	struct obj *objects[1][ROWNO];
	char *yuk1[COLNO - 1][ROWNO];
	struct monst *monsters[1][ROWNO];
	char *yuk2[COLNO - 1][ROWNO];
#endif
	struct obj *objlist;
	struct obj *buriedobjlist;
	struct monst *monlist;
	struct damage *damagelist;
	struct cemetery *bonesinfo;
	struct levelflags flags;
} dlevel_t;


/* Matt: This was also taken from mkmaze.c for reference to below.

Coord below is literally just int x and y.*/

/* find random starting point for maze generation */
STATIC_OVL void
maze0xy(cc)
coord *cc;
{
	cc->x = 3 + 2 * rn2((x_maze_max >> 1) - 1);
	cc->y = 3 + 2 * rn2((y_maze_max >> 1) - 1);
	return;
}


/* Matt: This was also taken from mkmaze.c for reference to okay below.
It is just a macro that moves the x,y in one of the specified directions.*/

/* adjust a coordinate one step in the specified direction */
#define mz_move(X, Y, dir) \
    do {                                                         \
        switch (dir) {                                           \
        case 0:  --(Y);  break;                                  \
        case 1:  (X)++;  break;                                  \
        case 2:  (Y)++;  break;                                  \
        case 3:  --(X);  break;                                  \
        default: panic("mz_move: bad direction %d", dir);        \
        }                                                        \
    } while (0) // Matt: Do once, what the hell?



/* Matt: This was also taken from mkmaze.c for reference in walkfrom() below.*/

STATIC_OVL boolean
okay(x, y, dir)
int x, y;
int dir;
{
	// Moves the x and y one place in the direction specified direction twice.
	mz_move(x, y, dir); 
	mz_move(x, y, dir);
	// Check for in bounds and that the x, y we moved too is of type stone, meaning
	// we have not yet carved out this spot.
	if (x < 3 || y < 3 || x > x_maze_max || y > y_maze_max
		|| levl[x][y].typ != STONE) 
		return FALSE;
	return TRUE;
}



/* Matt: This was also taken form mkmaze.c for reference for below.

This seems to be the real algorithm doing the generation work

I imagine we will only be worrying about the none MICRO defined version because
that seems to be for when stack space would be very limited.*/


#ifdef MICRO
/* Make the mazewalk iterative by faking a stack.  This is needed to
 * ensure the mazewalk is successful in the limited stack space of
 * the program.  This iterative version uses the minimum amount of stack
 * that is totally safe.
 */
void
walkfrom(x, y, typ)
int x, y;
schar typ;
{
#define CELLS (ROWNO * COLNO) / 4            /* a maze cell is 4 squares */
	char mazex[CELLS + 1], mazey[CELLS + 1]; /* char's are OK */
	int q, a, dir, pos;
	int dirs[4];

	if (!typ) {
		if (level.flags.corrmaze)
			typ = CORR;
		else
			typ = ROOM;
	}

	pos = 1;
	mazex[pos] = (char)x;
	mazey[pos] = (char)y;
	while (pos) {
		x = (int)mazex[pos];
		y = (int)mazey[pos];
		if (!IS_DOOR(levl[x][y].typ)) {
			/* might still be on edge of MAP, so don't overwrite */
			levl[x][y].typ = typ;
			levl[x][y].flags = 0;
		}
		q = 0;
		for (a = 0; a < 4; a++)
			if (okay(x, y, a))
				dirs[q++] = a;
		if (!q)
			pos--;
		else {
			dir = dirs[rn2(q)];
			mz_move(x, y, dir);
			levl[x][y].typ = typ;
			mz_move(x, y, dir);
			pos++;
			if (pos > CELLS)
				panic("Overflow in walkfrom");
			mazex[pos] = (char)x;
			mazey[pos] = (char)y;
		}
	}
}
#else /* !MICRO */

void
walkfrom(x, y, typ)
int x, y;
schar typ;
{
	int q, a, dir;
	int dirs[4];

	// Game specific settings.
	if (!typ) {
		if (level.flags.corrmaze)
			typ = CORR;
		else
			typ = ROOM; 
	}

	// If our x, y is not a door.
	if (!IS_DOOR(levl[x][y].typ)) {
			/* might still be on edge of MAP, so don't overwrite */
		levl[x][y].typ = typ; // Carve out a corridor.
		levl[x][y].flags = 0; // Clear game specific flags at this x, y.
	}

	while (1) {
		q = 0;
		// This loop checks in all 4 cardinal directions from this cell to see
		// if they are 'okay'.
		for (a = 0; a < 4; a++) 
			if (okay(x, y, a)) // We are okay in this direction.
				dirs[q++] = a; // Fill dirs with okay directions.

		// If q is zero at this point, we werent 'okay' in any direction so return.
		if (!q)
			return;
		dir = dirs[rn2(q)]; // Get a random okay direction.
		mz_move(x, y, dir); // Move one cell in that direction.
		levl[x][y].typ = typ; // 'Carve' a corridor.
		mz_move(x, y, dir); // Move that way again.
		walkfrom(x, y, typ); // Recursively call from the new spot.
	}
}
#endif /* ?MICRO */


/* Matt: This is the macro for ACCESSIBLE from rm.h used in maze_remove_deadends.
It checks the type against the enum for types. All values above the enum value for DOOR
are accessible spots.*/

#define ACCESSIBLE(typ) ((typ) >= DOOR) /* good position */


/* Matt: This is isok() taken from cmd.c for reference in maze_inbounds. It seems to check
that the provided x,y coords is within the COLNO and ROWNO of the terminal.*/

int
isok(x, y)
register int x, y;
{
	/* x corresponds to curx, so x==1 is the first column. Ach. %% */
	return x >= 1 && x <= COLNO - 1 && y >= 0 && y <= ROWNO - 1;
}


/* Matt: This is taken from mkmaze.c for reference in maze_remove_deadends()*/

boolean
maze_inbounds(x, y)
int x, y;
{
	// So we are maze inbounds if x and y are larger than 2, and less than the max,
	// and also isok(), which checks that they dont go out of the terminal size bounds?
	return (x >= 2 && y >= 2
		&& x < x_maze_max && y < y_maze_max && isok(x, y));
}


/* Matt: This is taken from mkmaze.c for reference to create_maze below
It is the function for pruning dead ends in the maze. */

// This function seems to essentially go through the maze, finding dead ends.
// It then picks one of these directions and knocks down the dead end.


// The typ parameter is game specific information and so can be ignored.
void
maze_remove_deadends(typ)
{
	char dirok[4];
	int x, y, dir, idx, idx2, dx, dy, dx2, dy2;

	dirok[0] = 0; /* lint suppression */
	for (x = 2; x < x_maze_max; x++) // Go through all maze x's from 2 up.
		for (y = 2; y < y_maze_max; y++) // Same for y's.

			// So if the spot we are at is an accessible type, and x and y are odd.
			if (ACCESSIBLE(levl[x][y].typ) && (x % 2) && (y % 2)) {
				idx = idx2 = 0;

				// We loop through 0 to 3 because NetHack represents the 4 cardinal directions
				// around the current cell using the values 0 to 3.
				for (dir = 0; dir < 4; dir++) {
					/* note: mz_move() is a macro which modifies
					   one of its first two parameters */
					dx = dx2 = x;
					dy = dy2 = y;
					// Move one cell in the current cardinal direction.
					mz_move(dx, dy, dir);
					if (!maze_inbounds(dx, dy)) { // If we are not within maze bounds.
						idx2++;
						continue;
					}
					// Move two cells in the current cardinal direction.
					mz_move(dx2, dy2, dir);
					mz_move(dx2, dy2, dir);
					if (!maze_inbounds(dx2, dy2)) { // If we are not maze inbounds.
						idx2++; 
						continue;
					}
					if (!ACCESSIBLE(levl[dx][dy].typ) // If we are not accessible at the 1st delta.
						&& ACCESSIBLE(levl[dx2][dy2].typ)) {// And we are accessible at the second delta.
						// Add this direction to dirok, notifying that there is a dead end eligible
						// for removal in this direction.
						dirok[idx++] = dir;
						idx2++;
					}
				}
				// Based on the situation in which idx and idx2 got incremented above, check if idx
				// is at least 1, indicating we found at least one direction with a dead end eligible to be
				// removed. Check that idx2 is at least 3 which indicated this is actually a dead end surrounded
				// on at least 3 directions by non-traversible terrain, instead of just a location in the middle
				// of a passage.
				if (idx2 >= 3 && idx > 0) {
					dx = x;
					dy = y;
					dir = dirok[rn2(idx)]; // randomly pick one of the ok dirs.
					mz_move(dx, dy, dir); // Move in that direction.
					levl[dx][dy].typ = typ; // Carve out a passage in place of the dead end.
				}
			}
}



/* Matt: I think this is the actual algorithm for the maze gen.*/

/* Create a maze with specified corridor width and wall thickness
 * TODO: rewrite walkfrom so it works on temp space, not levl
 */
void
create_maze(corrwid, wallthick)
int corrwid;
int wallthick;
{
	int x, y;
	coord mm;
	int tmp_xmax = x_maze_max; // Found in decl.h
	int tmp_ymax = y_maze_max;
	int rdx = 0;
	int rdy = 0;
	int scale;

	if (wallthick < 1)
		wallthick = 1; // Walls must be 1 thick at least
	else if (wallthick > 5)
		wallthick = 5; // 5 is wall thickness cap

	// Same as above for corridors.
	if (corrwid < 1)
		corrwid = 1;
	else if (corrwid > 5)
		corrwid = 5;

	// Matt: Question: What are these and why do they equal this.
	scale = corrwid + wallthick;
	rdx = (x_maze_max / scale);
	rdy = (y_maze_max / scale);

	/* This flag is set in makemaz() based on random rn2(3).
	Apparently it controls whether the maze is made up of corridors rather than
	ROOM?*/
	if (level.flags.corrmaze)  // Sp of the maze IS made up of corridors not ROOM.
		for (x = 2; x < (rdx * 2); x++) // Go from x2 to rdx*2
			for (y = 2; y < (rdy * 2); y++) // same for y
				levl[x][y].typ = STONE; // And set typ to stone.
	else
		for (x = 2; x <= (rdx * 2); x++) // same
			for (y = 2; y <= (rdy * 2); y++) // same
				// If x and y is odd, then set stone, otherwise a horizontal wall. why?
				levl[x][y].typ = ((x % 2) && (y % 2)) ? STONE : HWALL;

	/* set upper bounds for maze0xy and walkfrom */
	x_maze_max = (rdx * 2);
	y_maze_max = (rdy * 2);

	/* create maze */
	maze0xy(&mm);
	walkfrom((int)mm.x, (int)mm.y, 0); // call with mm's coords, which is a coord defined above
	// and set by the call to maze0xy, selecting a random start pos.

	if (!rn2(5)) // If call to random is 0.
		maze_remove_deadends((level.flags.corrmaze) ? CORR : ROOM);

	/* restore bounds */
	x_maze_max = tmp_xmax;
	y_maze_max = tmp_ymax;

	/* scale maze up if needed */
	if (scale > 2) {
		char tmpmap[COLNO][ROWNO];
		int rx = 1, ry = 1;

		/* back up the existing smaller maze */
		for (x = 1; x < x_maze_max; x++)
			for (y = 1; y < y_maze_max; y++) {
				tmpmap[x][y] = levl[x][y].typ;
			}

		/* do the scaling */
		rx = x = 2;
		while (rx < x_maze_max) {
			int mx = (x % 2) ? corrwid
				: ((x == 2 || x == (rdx * 2)) ? 1
					: wallthick);
			ry = y = 2;
			while (ry < y_maze_max) {
				int dx = 0, dy = 0;
				int my = (y % 2) ? corrwid
					: ((y == 2 || y == (rdy * 2)) ? 1
						: wallthick);
				for (dx = 0; dx < mx; dx++)
					for (dy = 0; dy < my; dy++) {
						if (rx + dx >= x_maze_max
							|| ry + dy >= y_maze_max)
							break;
						levl[rx + dx][ry + dy].typ = tmpmap[x][y];
					}
				ry += my;
				y++;
			}
			rx += mx;
			x++;
		}

	}
}

// if x is odd, mx = corrwid, if x is even (if x==2 or x == rdx*2, mx is 1, otherwise wallthick
int mx = (x % 2) ? corrwid : ((x == 2 || x == (rdx * 2)) ? 1 : wallthick);