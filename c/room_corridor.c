/* MATT: For reference in the rectangle array. This is the rectangle structure itself.*/

typedef struct nhrect {
	xchar lx, ly;
	xchar hx, hy;
} NhRect;


/* MATT: For reference in clear_level_structures() below. This seems
to be where to rectangle array is initialized. Each rectangle seems to be
simply defined by 2 points, it lowXY and highXY.
*/

/*
 * Initialisation of internal structures. Should be called for every
 * new level to be build...
 */

void
init_rect()
{
	rect_cnt = 1;
	rect[0].lx = rect[0].ly = 0;
	rect[0].hx = COLNO - 1;
	rect[0].hy = ROWNO - 1;
}



/* MATT: For reference relating to the creation of rooms. This function
is called in makelevel() before the actual room generation begins.
It just clears a bunch of global structures, what we care about is the
creation of the array of rectangles. I think this might be done in init_rect().*/

/* clear out various globals that keep information on the current level.
 * some of this is only necessary for some types of levels (maze, normal,
 * special) but it's easier to put it all in one place than make sure
 * each type initializes what it needs to separately.
 */
STATIC_OVL void
clear_level_structures()
{
	static struct rm zerorm = { cmap_to_glyph(S_stone),
								0, 0, 0, 0, 0, 0, 0, 0, 0 };
	register int x, y;
	register struct rm *lev;

	for (x = 0; x < COLNO; x++) {
		lev = &levl[x][0];
		for (y = 0; y < ROWNO; y++) {
			*lev++ = zerorm;
			/*
			 * These used to be '#if MICROPORT_BUG',
			 * with use of memset(0) for '#if !MICROPORT_BUG' below,
			 * but memset is not appropriate for initializing pointers,
			 * so do these level.objects[][] and level.monsters[][]
			 * initializations unconditionally.
			 */
			level.objects[x][y] = (struct obj *) 0;
			level.monsters[x][y] = (struct monst *) 0;
		}
	}
	level.objlist = (struct obj *) 0;
	level.buriedobjlist = (struct obj *) 0;
	level.monlist = (struct monst *) 0;
	level.damagelist = (struct damage *) 0;
	level.bonesinfo = (struct cemetery *) 0;

	level.flags.nfountains = 0;
	level.flags.nsinks = 0;
	level.flags.has_shop = 0;
	level.flags.has_vault = 0;
	level.flags.has_zoo = 0;
	level.flags.has_court = 0;
	level.flags.has_morgue = level.flags.graveyard = 0;
	level.flags.has_beehive = 0;
	level.flags.has_barracks = 0;
	level.flags.has_temple = 0;
	level.flags.has_swamp = 0;
	level.flags.noteleport = 0;
	level.flags.hardfloor = 0;
	level.flags.nommap = 0;
	level.flags.hero_memory = 1;
	level.flags.shortsighted = 0;
	level.flags.sokoban_rules = 0;
	level.flags.is_maze_lev = 0;
	level.flags.is_cavernous_lev = 0;
	level.flags.arboreal = 0;
	level.flags.wizard_bones = 0;
	level.flags.corrmaze = 0;

	nroom = 0;
	rooms[0].hx = -1;
	nsubroom = 0;
	subrooms[0].hx = -1;
	doorindex = 0;
	init_rect();
	init_vault();
	xdnstair = ydnstair = xupstair = yupstair = 0;
	sstairs.sx = sstairs.sy = 0;
	xdnladder = ydnladder = xupladder = yupladder = 0;
	made_branch = FALSE;
	clear_regions();
}





/* MATT: For reference in makerooms below. Just return a random rectangl
in the array of left over rectangle, that was created before the room gen started.*/

/*
 * Get some random NhRect from the list.
 */

NhRect *
rnd_rect()
{
	return rect_cnt > 0 ? &rect[rn2(rect_cnt)] : 0;
}



/* MATT: This is the function that actually does the room generation.*/

STATIC_OVL void
makerooms()
{
	// Game specific, ignore.
	boolean tried_vault = FALSE;

	/* make rooms until satisfied */
	/* rnd_rect() will returns 0 if no more rects are available... */

	// Here nroom is just a global for the number of rooms that have been placed.
	// rnd_rect returns a random rectangle from the global rectangle array.
	// So whilst we are lower than max rooms and we are still able to retrieve a random
	// rectangle.
	while (nroom < MAXNROFROOMS && rnd_rect()) {
		// This condition is just related to the creation of a vault room.
		// As this is game specific we can ignore.
		if (nroom >= (MAXNROFROOMS / 6) && rn2(2) && !tried_vault) {
			tried_vault = TRUE; // Again vault stuff.
			if (create_vault()) {
				vault_x = rooms[nroom].lx;
				vault_y = rooms[nroom].ly;
				rooms[nroom].hx = -1;
			}
		}
		// Here create a room. The -1 cause create_room to randomly generate the value.
		// OROOM is a part of an enum that denoted room types (Normal, court, wandshop etc.)
		// OROOM denotes an ordinary room.
		else if (!create_room(-1, -1, -1, -1, -1, -1, OROOM, -1))
			return; // Once we are failing to create rooms, drop out.
	}
	return;
}