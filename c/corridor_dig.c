/* Matt: This is the function for actually digging a corridor
between two points as is used in the join() function found
in the comment file corridors.c.
*/

/*
 * Dig a corridor between two points.
 */


// Matt: ftyp seems to be the type the floor of corridor should be.
// and btype the type the wall should be. Neither really bother us.
boolean
dig_corridor(org, dest, nxcor, ftyp, btyp)
coord *org, *dest;
boolean nxcor;
schar ftyp, btyp;
{
	int dx = 0, dy = 0, dix, diy, cct;
	struct rm *crm; // rm is a map tile in nethack.
	int tx, ty, xx, yy;

	xx = org->x;
	yy = org->y;
	tx = dest->x;
	ty = dest->y;

	// Out of bounds check.
	if (xx <= 0 || yy <= 0 || tx <= 0 || ty <= 0 || xx > COLNO - 1
		|| tx > COLNO - 1 || yy > ROWNO - 1 || ty > ROWNO - 1) {
		debugpline4("dig_corridor: bad coords <%d,%d> <%d,%d>.",
			xx, yy, tx, ty);
		return FALSE;
	}
	// Digging directions.
	if (tx > xx)
		dx = 1;
	else if (ty > yy)
		dy = 1;
	else if (tx < xx)
		dx = -1;
	else
		dy = -1;

	// Go in that direction from the origin point.
	// Matt: So here we minus the dx or dy from the origin point.
	// But just below when we enter the while loop we almost immediately add
	// the same value to the origin point. What the hell?
	xx -= dx;
	yy -= dy;
	cct = 0;
	// Matt: Whilst we arent at the target.
	while (xx != tx || yy != ty) {
		/* loop: dig corridor at [xx,yy] and find new [xx,yy] */

		// Matt: So, cct seems to be the amount of tiles we are willing to dig
		// before giving up.
		// Also, if nxcor is true, there is a 1 in 35 change that we will just
		// give up anyway. I suppose this is used to create NetHacks sea of corridors.
		if (cct++ > 500 || (nxcor && !rn2(35)))
			return FALSE;

		// Advance in the direction.
		// 
		xx += dx;
		yy += dy;

		// Matt: Bounds check! As the NetHack comment implies, I do believe
		// this should not be possible. As the target should never be outside
		// the bounds.
		if (xx >= COLNO - 1 || xx <= 0 || yy <= 0 || yy >= ROWNO - 1)
			return FALSE; /* impossible */

		crm = &levl[xx][yy];
		if (crm->typ == btyp) { // Matt: If the tyle is the supplied wall type.
			if (ftyp != CORR || rn2(100)) { // If we arent working on corridors. 99 out of 100
				crm->typ = ftyp; // Make a floor type.
				if (nxcor && !rn2(50)) // If nxcor and 1 in 50, also put a boulder here
					(void)mksobj_at(BOULDER, xx, yy, TRUE, FALSE);
			}
			else {
				crm->typ = SCORR;// Otherwise place a secret corridor.
			}
		}
		// Matt: Here the tile is not the supplied wall type?
		// so if the type is not the floor type, and not a secret corridor, exit?
		// I.e, we have run into something unexpected.
		else if (crm->typ != ftyp && crm->typ != SCORR) {
			/* strange ... */
			return FALSE;
		}

		// Matt: Here the absolute of xx-tx and same for y, so that we are always moving towards
		// the target.

		/* find next corridor position */
		dix = abs(xx - tx);
		diy = abs(yy - ty);

		// Matt: What is this?
		// Looks like some random chance that we wont move in one
		// of the specified directions.
		if ((dix > diy) && diy && !rn2(dix - diy + 1)) {
			dix = 0;
		}
		else if ((diy > dix) && dix && !rn2(diy - dix + 1)) {
			diy = 0;
		}

		/* do we have to change direction ? */

		// Matt: so if dy is specified, ie we are going that way, and the difference
		// we have totravel in x is greater than y.
		if (dy && dix > diy) {
			// Matt: Positive or negative x travel? I.e right or left.
			register int ddx = (xx > tx) ? -1 : 1;

			crm = &levl[xx + ddx][yy]; // The new position
			// If the type is equal to the wall, floor or secret corridor.
			if (crm->typ == btyp || crm->typ == ftyp || crm->typ == SCORR) {
				// dx is new position?
				// dy is no movement and continue?
				dx = ddx;
				dy = 0;
				continue;
			}
		}
		// Matt: Same as above but if we need to make ground in the y axis.
		else if (dx && diy > dix) {
			register int ddy = (yy > ty) ? -1 : 1;

			crm = &levl[xx][yy + ddy];
			if (crm->typ == btyp || crm->typ == ftyp || crm->typ == SCORR) {
				dy = ddy;
				dx = 0;
				continue;
			}
		}

		/* continue straight on? */
		crm = &levl[xx + dx][yy + dy];
		if (crm->typ == btyp || crm->typ == ftyp || crm->typ == SCORR)
			continue;

		/* no, what must we do now?? */
		if (dx) {
			dx = 0;
			dy = (ty < yy) ? -1 : 1;
		}
		else {
			dy = 0;
			dx = (tx < xx) ? -1 : 1;
		}
		crm = &levl[xx + dx][yy + dy];
		if (crm->typ == btyp || crm->typ == ftyp || crm->typ == SCORR)
			continue;
		dy = -dy;
		dx = -dx;
	}
	return TRUE;
}