/*Matt: Here is the define for rn1() as used below in create_rooms()*/

// Its a random within a random.
#define rn1(x, y) (rn2(x) + (y))



/*Matt: These are the defined for XLIM and YLIM used in create_room below.*/
#define XLIM 4
#define YLIM 3


/* MATT: This is the function that actually generates a single room, for reference in its
use in makerooms() in the room_corridor.c analysis file.*/

/*
 * Create a new room.
 * This is still very incomplete...
 */
boolean
create_room(x, y, w, h, xal, yal, rtype, rlit)
xchar x, y;
xchar w, h
xchar xal, yal;
xchar rtype, rlit;
{
	xchar xabs, yabs;
	int wtmp, htmp, xaltmp, yaltmp, xtmp, ytmp;
	// NhRect is just a rectangle struct, consisting of a low point and high point.
	NhRect *r1 = 0, r2;
	int trycnt = 0;
	boolean vault = FALSE;
	// XLIM and YLIM are global values in the NetHack source specific to this games
	// generation.
	int xlim = XLIM, ylim = YLIM;

	// Apparently if we ask for random room type we always get an ordinary room.
	if (rtype == -1) /* Is the type random ? */
		rtype = OROOM;

	// Ignore vault stuff.
	if (rtype == VAULT) {
		vault = TRUE;
		xlim++;
		ylim++;
	}

	// We aren't particuarly worried about light settings for
	// rooms when considering the procedural generation algorithms.

	/* on low levels the room is lit (usually) */
	/* some other rooms may require lighting */

	/* is light state random ? */
	if (rlit == -1)
		rlit = (rnd(1 + abs(depth(&u.uz))) < 11 && rn2(77)) ? TRUE : FALSE;

	/*
	 * Here we will try to create a room. If some parameters are
	 * random we are willing to make several try before we give
	 * it up.
	 */
	do { // Start of the generation loop, try 100 times.
		xchar xborder, yborder;
		wtmp = w;
		htmp = h;
		xtmp = x;
		ytmp = y;
		xaltmp = xal;
		yaltmp = yal;

		/* First case : a totally random room */

		if ((xtmp < 0 && ytmp < 0 && wtmp < 0 && xaltmp < 0 && yaltmp < 0)
			|| vault) {
			xchar hx, hy, lx, ly, dx, dy;
			r1 = rnd_rect(); /* Get a random rectangle */

			if (!r1) { /* No more free rectangles ! */
				debugpline0("No more rects...");
				return FALSE;
			}
			// Initialize the high and low points we are working with to that of the rect.
			hx = r1->hx;
			hy = r1->hy;
			lx = r1->lx;
			ly = r1->ly;
			if (vault) // Matt: Ignore vault stuff.
				dx = dy = 1;
			else {
				// Randomly generate the change in x of the room, with the maximum
				// modified base on the rectangels size.
				dx = 2 + rn2((hx - lx > 28) ? 12 : 8);
				dy = 2 + rn2(4); // Randomly generate change in y of room up to 4.

				// Matt: This is essentially clamping the overall room square space to
				// lower than 50 by scaling the y accordingly.
				if (dx * dy > 50)
					dy = 50 / dx;
			}
			// These values represent the border that is required between the room
			// being generated and the rectangle it is being generated inside.
			// This is full of just game specific constants.
			xborder = (lx > 0 && hx < COLNO - 1) ? 2 * xlim : xlim + 1;
			yborder = (ly > 0 && hy < ROWNO - 1) ? 2 * ylim : ylim + 1;

			// If the new room doesn't fit within the rectangle plus the border
			// values then we exit this attempt and try again.
			if (hx - lx < dx + 3 + xborder || hy - ly < dy + 3 + yborder) {
				r1 = 0;
				continue;
			}
			// This calculated the x absolute and y absolute, which means the actualy point
			// at which we are going to place the low point of the rooms rectangle.
			// Once again a whole lot of game specific constants.
			xabs = lx + (lx > 0 ? xlim : 3)
				+ rn2(hx - (lx > 0 ? lx : 3) - dx - xborder + 1);
			// Same stuff for the abs y.
			yabs = ly + (ly > 0 ? ylim : 2)
				+ rn2(hy - (ly > 0 ? ly : 2) - dy - yborder + 1);

			// This seems to just be a set of game specifiec modification to generation.

			// If the low y of the rect is == 0, and they hy is greater than the ROWNO -1,
			// and we have not generated a room yet (!nroom), or some random chance based on the
			// No rooms already generated (!rn2(nroom)) and the abs y plus the change in y
			// is greater than half of the total rows.
			// The abs y is changed to some random value up to 2, plus 2?
			if (ly == 0 && hy >= (ROWNO - 1) && (!nroom || !rn2(nroom))
				&& (yabs + dy > ROWNO / 2)) {
				yabs = rn1(3, 2);

				// Matt: Then if the number of rooms already is less than 4, and the change in y
				// is greater than one, take 1 from the change in y.
				if (nroom < 4 && dy > 1)
					dy--;
			}

			// Here we call check room to make sure it fits in the rectangle.
			// We give pointers because this will modify the sizes we give,
			// and return true if it gets a fit and false if not.
			if (!check_room(&xabs, &dx, &yabs, &dy, vault)) {
				r1 = 0;
				continue;
			}
			wtmp = dx + 1;
			htmp = dy + 1;
			r2.lx = xabs - 1; // Matt: Set low x of rec2 to room x -1
			r2.ly = yabs - 1; // Samve for low y.
			r2.hx = xabs + wtmp; // High x to our x pos, + our width + 1 as above.
			r2.hy = yabs + htmp; // Same for y.
		}
		// Matt: All the same crap for some hard values.
		else { /* Only some parameters are random */
			int rndpos = 0;
			if (xtmp < 0 && ytmp < 0) { /* Position is RANDOM */
				xtmp = rnd(5);
				ytmp = rnd(5);
				rndpos = 1;
			}
			if (wtmp < 0 || htmp < 0) { /* Size is RANDOM */
				wtmp = rn1(15, 3);
				htmp = rn1(8, 2);
			}
			if (xaltmp == -1) /* Horizontal alignment is RANDOM */
				xaltmp = rnd(3);
			if (yaltmp == -1) /* Vertical alignment is RANDOM */
				yaltmp = rnd(3);

			/* Try to generate real (absolute) coordinates here! */

			xabs = (((xtmp - 1) * COLNO) / 5) + 1;
			yabs = (((ytmp - 1) * ROWNO) / 5) + 1;
			switch (xaltmp) {
			case LEFT:
				break;
			case RIGHT:
				xabs += (COLNO / 5) - wtmp;
				break;
			case CENTER:
				xabs += ((COLNO / 5) - wtmp) / 2;
				break;
			}
			switch (yaltmp) {
			case TOP:
				break;
			case BOTTOM:
				yabs += (ROWNO / 5) - htmp;
				break;
			case CENTER:
				yabs += ((ROWNO / 5) - htmp) / 2;
				break;
			}

			if (xabs + wtmp - 1 > COLNO - 2)
				xabs = COLNO - wtmp - 3;
			if (xabs < 2)
				xabs = 2;
			if (yabs + htmp - 1 > ROWNO - 2)
				yabs = ROWNO - htmp - 3;
			if (yabs < 2)
				yabs = 2;

			/* Try to find a rectangle that fit our room ! */

			r2.lx = xabs - 1;
			r2.ly = yabs - 1;
			r2.hx = xabs + wtmp + rndpos;
			r2.hy = yabs + htmp + rndpos;
			r1 = get_rect(&r2);
		}
	} while (++trycnt <= 100 && !r1); // Try 100 times and r1 is 0 as it could be set above.
	if (!r1) { /* creation of room failed ? */
		return FALSE;
	}
	// Split the rects in the array now to deal with the room taking one.
	split_rects(r1, &r2);

	// This is our case, no vault.
	if (!vault) {
		// No idea what smeq is.
		// But seems to be an array of empty structs, up to max rooms + 1
		smeq[nroom] = nroom;
		// Add the room to all the structs etc.
		add_room(xabs, yabs, xabs + wtmp - 1, yabs + htmp - 1, rlit, rtype,
			FALSE);
	}
	// If a vault so ignore.
	else {
		rooms[nroom].lx = xabs;
		rooms[nroom].ly = yabs;
	}
	// Done.
	return TRUE;
}



// Matt: add_room as called above.

void
add_room(lowx, lowy, hix, hiy, lit, rtype, special)
int lowx, lowy, hix, hiy;
boolean lit;
schar rtype;
boolean special;
{
	register struct mkroom *croom;

	croom = &rooms[nroom];
	do_room_or_subroom(croom, lowx, lowy, hix, hiy, lit, rtype, special,
		(boolean)TRUE);
	croom++;
	croom->hx = -1;
	nroom++;
}


// Matt: do_room_or_subroom as called in add_room above.

STATIC_OVL void
do_room_or_subroom(croom, lowx, lowy, hix, hiy, lit, rtype, special, is_room)
register struct mkroom *croom;
int lowx, lowy;
register int hix, hiy;
boolean lit;
schar rtype;
boolean special;
boolean is_room;
{
	register int x, y;
	struct rm *lev;

	/* locations might bump level edges in wall-less rooms */
	/* add/subtract 1 to allow for edge locations */
	if (!lowx)
		lowx++;
	if (!lowy)
		lowy++;
	if (hix >= COLNO - 1)
		hix = COLNO - 2;
	if (hiy >= ROWNO - 1)
		hiy = ROWNO - 2;

	if (lit) {
		for (x = lowx - 1; x <= hix + 1; x++) {
			lev = &levl[x][max(lowy - 1, 0)];
			for (y = lowy - 1; y <= hiy + 1; y++)
				lev++->lit = 1;
		}
		croom->rlit = 1;
	}
	else
		croom->rlit = 0;

	croom->lx = lowx;
	croom->hx = hix;
	croom->ly = lowy;
	croom->hy = hiy;
	croom->rtype = rtype;
	croom->doorct = 0;
	/* if we're not making a vault, doorindex will still be 0
	 * if we are, we'll have problems adding niches to the previous room
	 * unless fdoor is at least doorindex
	 */
	croom->fdoor = doorindex;
	croom->irregular = FALSE;

	croom->nsubrooms = 0;
	croom->sbrooms[0] = (struct mkroom *) 0;
	if (!special) {
		for (x = lowx - 1; x <= hix + 1; x++)
			for (y = lowy - 1; y <= hiy + 1; y += (hiy - lowy + 2)) {
				levl[x][y].typ = HWALL;
				levl[x][y].horizontal = 1; /* For open/secret doors. */
			}
		for (x = lowx - 1; x <= hix + 1; x += (hix - lowx + 2))
			for (y = lowy; y <= hiy; y++) {
				levl[x][y].typ = VWALL;
				levl[x][y].horizontal = 0; /* For open/secret doors. */
			}
		for (x = lowx; x <= hix; x++) {
			lev = &levl[x][lowy];
			for (y = lowy; y <= hiy; y++)
				lev++->typ = ROOM;
		}
		if (is_room) {
			levl[lowx - 1][lowy - 1].typ = TLCORNER;
			levl[hix + 1][lowy - 1].typ = TRCORNER;
			levl[lowx - 1][hiy + 1].typ = BLCORNER;
			levl[hix + 1][hiy + 1].typ = BRCORNER;
		}
		else { /* a subroom */
			wallification(lowx - 1, lowy - 1, hix + 1, hiy + 1);
		}
	}
}




/* Matt: The check_room function for reference to its call above in create_room.*/


boolean
check_room(lowx, ddx, lowy, ddy, vault)
xchar *lowx, *ddx, *lowy, *ddy;
boolean vault;
{
	register int x, y, hix = *lowx + *ddx, hiy = *lowy + *ddy;
	register struct rm *lev;
	int xlim, ylim, ymax;

	// Matt: Ignore this vault limit stuff.
	xlim = XLIM + (vault ? 1 : 0);
	ylim = YLIM + (vault ? 1 : 0);

	// If the rooms lowx has ended up less than 3, set it to 3.
	if (*lowx < 3)
		*lowx = 3;
	// If the rooms lowy has ended up less than 2, set it to 2.
	if (*lowy < 2)
		*lowy = 2;
	// Matt: If our copy of the rooms high x has ended up greater than the total number of
	// columns - 3, then set it to that value.
	if (hix > COLNO - 3)
		hix = COLNO - 3;
	// Matt: Same as above for y.
	if (hiy > ROWNO - 3)
		hiy = ROWNO - 3;
chk: // Label
	// Matt: If our copy of the high x, is less than or equal to the rooms lowx or same
	// for the y, Then return false for now fit. This would mean we have a point or inverted room somehow.
	if (hix <= *lowx || hiy <= *lowy)
		return FALSE;

	/* check area around room (and make room smaller if necessary) */

	// Matt: loop through from the low x value of the room, and xlim squared below it.
	// So lim values are the area around that must be clear.
	// And loop up to the high x plus the lim over.
	for (x = *lowx - xlim; x <= hix + xlim; x++) {
		// Matt: If x ended up out of bounds we are by the edge of the map, continue.
		if (x <= 0 || x >= COLNO)
			continue;
		// Matt: The y value starting at lowy and the lim under.
		y = *lowy - ylim;
		// Matt: Same for y max.
		ymax = hiy + ylim;
		// Matt: If the y ended up at less than 0, we cap at 0 instead of continuing for some reason.
		if (y < 0)
			y = 0;
		// Matt: Same for the high y and the max rows.
		if (ymax >= ROWNO)
			ymax = (ROWNO - 1);
		lev = &levl[x][y]; // Matt: Get the current level map from global.
		// Matt: For all the y's we set above, go until we get to the max y's.
		for (; y <= ymax; y++) {
			// Matt: If this spots tile type is anything other than stone.
			if (lev++->typ) {
				// Matt: This is us as we arent doing vaults. So it appears if we pick
				// up a point type that isnt stone and we are not in a vaule, its a strange area?
				if (!vault) {
					debugpline2("strange area [%d,%d] in check_room.", x, y);
				}
				// Matt: If we have found a type that is not stone, and for some random value
				// up to 2, exit with False? Why????
				if (!rn2(3))
					return FALSE;
				// Matt: It we hit a type that is not stone, and our current x is less than
				// the lowx of the room, so still out its x borders then set the low x of the
				// actual room to, the current x plus the lim value plus one.
				// So this is like, we hit something on the low x side so resize.
				if (x < *lowx)
					*lowx = x + xlim + 1;
				// Matt: Otherwise, our temporary high x is equal to the current x, minus
				// the lim - 1, so this is the same thing on the high x side.
				else
					hix = x - xlim - 1;
				// Matt: Same as above for y.
				if (y < *lowy)
					*lowy = y + ylim + 1;
				else
					hiy = y - ylim - 1;
				goto chk; // Go back to the check lable to start the process again.
			}
		}
	}
	// Matt: Okay now with our temporary high x/y values, we can modify the
	// rooms actual width and height to match.
	*ddx = hix - *lowx;
	*ddy = hiy - *lowy;
	return TRUE; // Matt: yay we fit.
}


/* Matt: This is split rects for reference in create_room above.*/

/*
 * Okay, here we have two rectangles (r1 & r2).
 * r1 was already in the list and r2 is included in r1.
 * What we want is to allocate r2, that is split r1 into smaller rectangles
 * then remove it.
 */

void
split_rects(r1, r2)
NhRect *r1, *r2; // Matt: Pointers to the 2 rects.
{
	NhRect r, old_r;
	int i;

	old_r = *r1; // Matt: The old rect is the first one
	remove_rect(r1); // Get rid of old rect

	/* Walk down since rect_cnt & rect[] will change... */
	for (i = rect_cnt - 1; i >= 0; i--) // Matt: Loop down the array. Why down?
		// Ah it goes down because we are adding on to the end of rect as we go through.
		// Matt: Check if the current rect in array, intersect with our second
		// rect(the room rect) and put the intersection into r.
		if (intersect(&rect[i], r2, &r))
			// Matt: Recurse ourselves with the current processing rect and
			// the intersecting rect.
			split_rects(&rect[i], &r);


	// Matt: So, if r2's lower y minus the old rects lower y minus 1, is greater than
	// either 2 * YLIM or yLIM + 1 plus 4, depending on if the high y of the old rect
	// is less than the max rows -1.
	if (r2->ly - old_r.ly - 1
				> (old_r.hy < ROWNO - 1 ? 2 * YLIM : YLIM + 1) + 4) {
		// then intersecting rect now is the old rect.
		r = old_r;
		// intersecting rect high y is r2 lower y - 2.
		r.hy = r2->ly - 2;
		// add the 
		add_rect(&r); // Add the r to the liest/
	}

	// Matt: Same as above but for all of the x's
	if (r2->lx - old_r.lx - 1
				> (old_r.hx < COLNO - 1 ? 2 * XLIM : XLIM + 1) + 4) {
		r = old_r;
		r.hx = r2->lx - 2;
		add_rect(&r);
	}
	// Matt: If the old rects high y minus r2's high y minus 1 is greater than
	// the YLIM values as based on old rects ly being greater than 0, plus 4.
	if (old_r.hy - r2->hy - 1 > (old_r.ly > 0 ? 2 * YLIM : YLIM + 1) + 4) {
		// Similar to the y but for x.
		r = old_r;
		r.ly = r2->hy + 2;
		add_rect(&r);
	}
	// Same as above but for high x.
	if (old_r.hx - r2->hx - 1 > (old_r.lx > 0 ? 2 * XLIM : XLIM + 1) + 4) {
		r = old_r;
		r.lx = r2->hx + 2;
		add_rect(&r);
	}
}


// Matt: Here is remove rectangle as used in split rects.
/*NOTE: This is really important! Where as my original port
of the code just removed the rect from the list, and so the length of
the list would decrement. So when we popped back up the stack we would
get out of range. This remove just assigned the end of the array to the
index location we wish to remove. So the end moves to the new index.
But when we pop up the stack in split_rects(), we still have access to
the same elements at the end of the array and don't out of range.*/

/*
* Remove a rectangle from the list of free NhRect.
*/

void
remove_rect(r)
NhRect *r;
{
	int ind;

	ind = get_rect_ind(r);
	if (ind >= 0)
		rect[ind] = rect[--rect_cnt];
}



// Matt: This is intersect as used in split rect.

/*
 * Search intersection between two rectangles (r1 & r2).
 * return TRUE if intersection exist and put it in r3.
 * otherwise returns FALSE
 */

STATIC_OVL boolean
intersect(r1, r2, r3)
NhRect *r1, *r2, *r3;
{
	// Matt: This essentially checks the corners of the rects against each
	// other for impossibility to conntect. See Ref interect rect in notebook
	// for the dimensional rational.
	if (r2->lx > r1->hx || r2->ly > r1->hy || r2->hx < r1->lx
		|| r2->hy < r1->ly)
		return FALSE;

	
	// Here we actually assigne the intersecting rectangle to 3.
	// See ref get intersect in notebook for rational.
	r3->lx = (r2->lx > r1->lx ? r2->lx : r1->lx);
	r3->ly = (r2->ly > r1->ly ? r2->ly : r1->ly);
	r3->hx = (r2->hx > r1->hx ? r1->hx : r2->hx);
	r3->hy = (r2->hy > r1->hy ? r1->hy : r2->hy);

	// I think we get this inverted sqare condition only if the
	// rects werent actually colliding, so should we ever actually hit this
	// considering the check for possible intersect above.
	// Matt: Well yes ofcourse because those are only corner checks and it is based on
	// if this corner or this corner. It doesn't do a full space check. Its possible
	// that the corners wont end up like that but still the rects are just distanced
	// far enough apart there is no connect.
	if (r3->lx > r3->hx || r3->ly > r3->hy)
		return FALSE;
	return TRUE; // Connected
}


// Matt: The add rect as called in split_rect

/*
* Add a NhRect to the list.
*/

void
add_rect(r)
NhRect *r;
{
	if (rect_cnt >= MAXRECT) {
		if (wizard)
			pline("MAXRECT may be too small.");
		return;
	}
	/* Check that this NhRect is not included in another one */
	if (get_rect(r))
		return;
	rect[rect_cnt] = *r;
	rect_cnt++;
}


// Matt this is the get_rect called in add_rect above.
// if finds a rect that contains the supplied rect if it exists.

/*
 * Search a free rectangle that include the one given in arg
 */

NhRect *
get_rect(r)
NhRect *r;
{
	register NhRect *rectp;
	register int lx, ly, hx, hy;
	register int i;

	lx = r->lx;
	ly = r->ly;
	hx = r->hx;
	hy = r->hy;
	// Matt: Go through all rects.
	for (i = 0, rectp = &rect[0]; i < rect_cnt; i++, rectp++)
		// Bounds checks for containing.
		if (lx >= rectp->lx && ly >= rectp->ly && hx <= rectp->hx
			&& hy <= rectp->hy)
			return rectp;
	return 0;
}