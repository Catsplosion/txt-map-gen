# txt-map-gen: Procedural level and map generation

This is the README for my dissertation project txt-map-gen(WIP).

In this project we are examining the procedural generation code present in the NetHack source code (https://github.com/NetHack/NetHack).
The methods and algorithms used here will then be re implemented within a python command line tool designed to output text
files with procedurally generated maps and levels in them for further use.

## Analysis Notes

### Bounds overrun when scaling up maze

I have got the test bed for the scaling basically working, although it doesn't
work for certain ratios of the wall and corridor size with respect to the overall
maze size.

I need to figure out why this is. Maybe it't a limit not commented in the original
code, or maybe mine is broken in some way. I can see that it is because the ratios
don't fit though thanks to the testbed. Maybe this is helped by the buffer of stone
that the original code has around the maze that I removed. Hopefully for the real
implementation we will see.

### Difference between maze gen with CORR or ROOM

As far as I can tell, the only difference here is if it using ROOM, the maze gets `wallified`.
This essentially does the splining so instead of just corridor floor for the maze like:

```
######
     #
     #
     #######
```

type layout for a maze, you get:

```
----------------|
                |
--------------  |
             |  |
             |  |
             |  |
             |   ------------
             |
             ----------------
```