from copy import copy

from procGen.point_map import PointType


POINT_CHAR_MAPPING = {
    PointType.CORRIDOR: ' ',
    PointType.STONE: ' ',
    PointType.ROOM: ' ',
    PointType.DOOR: '.',
    PointType.CORRIDOR_WALL: '#',
    PointType.BR_CORNER: '#',
    PointType.TR_CORNER: '#',
    PointType.TL_CORNER: '#',
    PointType.BL_CORNER: '#',
    PointType.VERT_WALL: '#',
    PointType.HORIZ_WALL: '#'
}


def write_maze(maze, output_path):
    preamble = 'define 1 room 1'
    map_char = str(maze)
    numbered = map_char[:maze.top_left] + '1' + map_char[maze.top_left + 1:]

    output = preamble + '\n' + '\n' + numbered

    with open(output_path, 'w') as f:
        f.write(output)


def write_room_corridor(room_corridor, output_path):
    rooms = copy(room_corridor.rooms)
    corridors = copy(room_corridor.corridor_top_lefts)

    all_count = len(rooms) + len(corridors)

    letters = []
    if all_count >= 10:
        for i in range(0, all_count - 9):
            letters.append(chr(97 + i))

    if letters:
        number_cap = 9
    else:
        number_cap = all_count

    preamble = ''

    for i in range(0, number_cap):
        preamble += 'define ' + str(i + 1) + ' room ' + str(i + 1) + '\n'
    for i, letter in enumerate(letters):
        preamble += 'define ' + letter + ' room ' + str(number_cap + i + 1) + '\n'

    room_corridor.left_justify()
    max_x = room_corridor.max_x
    max_y = room_corridor.max_y

    map_point = room_corridor.point_map
    map_char = ''
    for y in range(0, max_y):
        map_char += '\n'
        for x in range(0, max_x):
            map_char += POINT_CHAR_MAPPING[map_point[x, y]]

    iterations = 1

    while corridors:
        if letters:
            index = letters.pop()
        else:
            index = iterations
            iterations += 1

        position = corridors.pop()
        top_left = ((position[1] + 1) * max_x + (position[0] + 1)) + position[1] + 1
        index_string = str(index)
        map_char = map_char[:top_left] + index_string + map_char[top_left + 1:]

    while rooms:
        if letters:
            index = letters.pop()
        else:
            index = iterations
            iterations += 1

        room = rooms.pop()
        top_left = ((room.low_y + 1) * max_x + (room.low_x + 1)) + room.low_y + 2
        index_string = str(index)
        map_char = map_char[:top_left] + index_string + map_char[top_left + 1:]

    output = preamble + map_char

    with open(output_path, 'w') as f:
        f.write(output)
