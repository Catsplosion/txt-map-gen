import random


# A function to return a random integer between 0 and x.
# This is made for easier compliance to the NetHack code
# As it is exclusive of the upper bound. I.E:
# 0 < excl_rand(x) < x

def _excl_rand(x):
    assert(x > 0)
    return random.randint(0, x - 1)
