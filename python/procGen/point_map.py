from enum import Enum, unique

@unique
class PointType(Enum):
    STONE = 0
    ROOM = 1
    CORRIDOR = 2
    CORRIDOR_WALL = 3
    HORIZ_WALL = 4
    VERT_WALL = 5
    TL_CORNER = 6
    TR_CORNER = 7
    BL_CORNER = 8
    BR_CORNER = 9
    DOOR = 10

    def __str__(self):
        return self.value

    def __repr__(self):
        class_name = type(self).__name__
        return '{}({})'.format(class_name, self.name)


class PointMap:
    def __init__(self, max_x, max_y):
        self.max_x = max_x
        self.max_y = max_y
        self.map = [[PointType.STONE] * self.max_x
                    for i in range(0, self.max_y)]

    def __str__(self):
        mapping = [['#'] * self.max_x for i in range(0, self.max_y)]
        for y in range(0, self.max_y):
            for x in range(0, self.max_x):
                mapping[y][x] = str(self.map[y][x])
        return '\n'.join([''.join([str(cell) for cell in row])
                          for row in mapping])

    def __repr__(self):
        class_name = type(self).__name__
        return '{}({}, {})'.format(class_name, self.max_x, self.max_y)

    def __getitem__(self, xy_tuple):
        x, y = xy_tuple
        return self.map[y][x]

    def __setitem__(self, xy_tuple, value):
        x, y, = xy_tuple
        self.map[y][x] = value
