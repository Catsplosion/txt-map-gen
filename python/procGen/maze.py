from .general import _excl_rand


class Maze:
    # region Public-interface
    def __init__(self, x=20, y=40, corridor_width=1, wall_width=1):
        """The constructor for the Maze class used to generate a maze type level.

        You can use the maze class to generate a perfect maze under a certain
        x and y value, with corridors and walls of specified width.

        :param x: The maximum x dimension of the maze
        :param y: The maximum y dimension of the maze
        :param corridor_width: The width of the mazes corridors
        :param wall_width: The width of the mazes walls
        """
        self.x_max = x
        self.y_max = y

        # region Parameter limits enforced in the NetHack code.
        # TODO: Evaluate these.
        # TODO: Evaluate moving these limits in __setattr__
        # TODO: Evaluation seems to show that you cant go over half either
        # the max_x or max_y in total scale.
        if corridor_width < 1:
            self.corr_width = 1
        elif corridor_width > 5:
            self.corr_width = 5
        else:
            self.corr_width = corridor_width

        if wall_width < 1:
            self.wall_width= 1
        elif wall_width> 5:
            self.wall_width= 5
        else:
            self.wall_width= wall_width
        # endregion

        self._scale = self.corr_width + self.wall_width
        self._rdx = (self.x_max // self._scale) * 2
        self._rdy = (self.y_max // self._scale) * 2
        # TODO: Can I just set this maze to rdx/rdy?
        self._down_scale_maze = [['#'] * (self.y_max + 1) for i in range(0, self.x_max + 1)]

        # The size that the final scaled up maze axes will be is represented
        # by a hyperbolic paraboloid function of the corridor and wall widths,
        # and the rdx, rxy values.
        self.maze = [['!'] * (self.corr_width *
                              (self._rdy // 2 - 1) +
                              self.wall_width *
                              (self._rdy // 2 - 1)
                              - self.wall_width + 2) \
                     for i in range(0,
                                    (self.corr_width *
                                     (self._rdx // 2 - 1) +
                                     self.wall_width *
                                     (self._rdx // 2 - 1) -
                                     self.wall_width + 2))]

        self.top_left = self.corr_width * (self._rdy // 2 - 1) + self.wall_width * (self._rdy // 2 - 1) + 3

    def __str__(self):
        return '\n'.join([''.join([str(cell) for cell in row])
                          for row in self.maze])

    def __repr__(self):
        class_name = type(self).__name__
        return '{}({}, {}, {}, {})'.format(class_name, self.x_max, self.y_max,
                                           self.corr_width, self.wall_width)

    def generate_maze(self):
        """ This function will generate a maze with the specified parameters,
        filling the maze data structure.

        The maze data structure will be filled and can be accessed to get to the
        maze. A value of top_left will also be assigned which specifies the first
        floor tile in the top left of the final maze.
        """
        start_pos = self._gen_start_pos()
        self._walk(start_pos)

        self._scale_up()

    def remove_dead_ends(self):
        """ Removes dead ends throughout the maze.

        This function will remove any dead ends it finds in the maze data
        structure. The data structure is modified in place.
        """
        dir_ok = []
        for x in range(2, self._rdx):
            for y in range(2, self._rdy):
                # We only perform this if the space is also odd along both
                # axes because the algorithm produces a perfect maze with
                # 3x3 being a floor space, we can know that any even space
                # will not have a dead end even if it is floor.
                if self._down_scale_maze[x][y] == ' ' and (x % 2) and (y % 2):
                    idx = idx2 = 0
                    # Loop the four cardinal directions.
                    for direction in range(0, 4):
                        pos1 = [x, y]
                        pos2 = [x, y]
                        self._move(pos1, direction)
                        # Bounds hit so may still be a dead end.
                        if not self._in_bounds(pos1[0], pos1[1]):
                            idx2 += 1
                            continue
                        # Move pos2 two steps in the direction to check the
                        # other side of the possible dead end to remove.
                        self._move(pos2, direction)
                        self._move(pos2, direction)
                        # Bounds hit so may still be a dead end.
                        if not self._in_bounds(pos2[0], pos2[1]):
                            idx2 += 1
                            continue
                        # At least one side we can knock down, and may stil be a
                        # dead end.
                        if self._down_scale_maze[pos1[0]][pos1[1]] == '#' \
                                and self._down_scale_maze[pos2[0]][pos2[1]] == ' ':
                            dir_ok.insert(idx, direction)
                            idx += 1
                            idx2 += 1


                    # idx must be > 0 to indicate we actually found a clear space
                    # on the other side of a wall at least once.
                    # Idx2 must be >= 3 to indicate, either this space is
                    # surrounded by 3 walls with spaces on the other side,
                    # indicating a dead end, or it has between one and two
                    # of those, and also bumps up against 1 or 2 of the mazes
                    # bounds.
                    if idx2 >= 3 and idx > 0:
                        pos1[0] = x
                        pos1[1] = y
                        move_direction = dir_ok[_excl_rand(idx)]
                        self._move(pos1, move_direction)
                        self._down_scale_maze[pos1[0]][pos1[1]] = ' '
    # endregion

    # region Private-interface
    def _gen_start_pos(self):
        # Find the starting position to dig first.
        # 3 ensures we are in the border as does -1.
        # The division and multiplication ensure we
        # start in a spot that will allow our perfect
        # maze generation with corridor at 3x3
        # so we can have odd axis indices as corridor width
        # and even as wall width.
        x = 3 + 2 * _excl_rand(self._rdx // 2 - 1)
        y = 3 + 2 * _excl_rand(self._rdy // 2 - 1)
        return x, y

    # TODO: Consider just moving this out of the class?
    @staticmethod
    def _move(position, direction):
        # This will shift the point list in place in a cardinal direction.
        if direction == 0:
            position[1] -= 1
        elif direction == 1:
            position[0] += 1
        elif direction == 2:
            position[1] += 1
        elif direction == 3:
            position[0] -= 1

    def _okay(self, position, direction):
        # The okay check for walking the maze. We move forward 2 steps
        # in the direction and check for bounds overrun or collision with
        # previously dug tiles. We check 2 spaces ahead because we are always
        # checking with one tile latency compared to what we modify due to
        # always clearing the start position.
        pos = position.copy()
        self._move(pos, direction)
        self._move(pos, direction)

        if pos[0] < 3 or pos[1] < 3 or pos[0] > (self._rdx) or \
                pos[1] > (self._rdy) or self._down_scale_maze[pos[0]][pos[1]] != '#':
            return False
        else:
            return True



    def _walk(self, start_pos):
        # Recursively o the walking algorithm
        # throughout the maze producing the corridor tiles. When we hit a not
        # okay condition so we cant go further, it will recurse up through the
        # tiles, starting a new recursion path for the next available
        # direction.
        my_coordinate = list(start_pos)
        # TODO: Check for door here to actually place the maze point.
        # For now not implemented because we have no doors.
        # So for now just place the first point without check.
        self._down_scale_maze[my_coordinate[0]][my_coordinate[1]] = ' '

        while True:
            dirs = []
            good_dir_count = 0
            for direction in range(0, 4):
                if self._okay(my_coordinate, direction):
                    dirs.insert(good_dir_count, direction)
                    good_dir_count += 1

            if not good_dir_count:
                return

            walk_direction = dirs[_excl_rand(good_dir_count)]
            self._move(my_coordinate, walk_direction)
            self._down_scale_maze[my_coordinate[0]][my_coordinate[1]] = ' '
            self._move(my_coordinate, walk_direction)
            self._walk(my_coordinate)





    def _scale_up(self):
        # If scale is 2 then room and corridor size is set to 1, so no scaling
        # just a straight copy.
        if self._scale <= 2:
            rx = 0
            x = 2
            while x <= self._rdx:
                ry = 0
                y = 2
                while y <= self._rdy:
                    self.maze[rx][ry] = self._down_scale_maze[x][y]
                    y += 1
                    ry += 1
                rx += 1
                x += 1
        # Scaling
        # This relies on the fact that the unscaled maze will be a perfect maze
        # with the 3x3 element being a corridor, so we can scale correctly across
        # the axis to achieve "width" and not just a scale across both axes
        # because we know each odd index for both axes will need to be the
        # assigned corridor width, and the wall width for even indices.
        else:
            rx = 0
            x = 2
            while x <= self._rdx:
                if x == 2 or x == self._rdx:
                    mx = 1
                elif x % 2:
                    mx = self.corr_width
                else:
                    mx = self.wall_width
                ry = 0
                y = 2
                while y <= self._rdy:
                    if y == 2 or y == self._rdy:
                        my = 1
                    elif y % 2:
                        my = self.corr_width
                    else:
                        my = self.wall_width

                    for dx in range(0, mx):
                        for dy in range(0, my):
                            self.maze[rx + dx][ry + dy] = self._down_scale_maze[x][y]
                    ry += my
                    y += 1
                rx += mx
                x += 1

    def _in_bounds(self, x, y):
        return 2 <= x < self._rdx and 2 <= y < self._rdy

    # endregion

    # region debug
    def _print_maze(self):
        print('\n'.join([' '.join([str(cell) for cell in row])
                         for row in self._down_scale_maze]))

    def _print_final_maze(self):
        print('\n'.join([' '.join([str(cell) for cell in row])
                         for row in self.maze]))
    # endregion debug
