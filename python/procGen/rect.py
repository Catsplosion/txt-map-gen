class Rect:
    def __init__(self, low_x=0, low_y=0, high_x=0, high_y=0):
        self.low_x = low_x
        self.low_y = low_y
        self.high_x = high_x
        self.high_y = high_y

    def __str__(self):
        return '({}, {}) to ({}, {})'.format(self.low_x, self.low_y,
                                             self.high_x, self.high_y)

    def __repr__(self):
        class_name = type(self).__name__
        return'{}({}, {}, {}, {})'.format(class_name, self.low_x, self.low_y,
                                          self.high_x, self.high_y)

    def graph(self):
        diff_x = self.high_x - self.low_x
        diff_y = self.high_y - self.low_y
        join_string = ['#' * diff_x]
        for i in range(0, diff_y - 2):
            join_string.append('#' + ' ' * (diff_x - 2) + '#')
        join_string.append('#' * diff_x)
        return '\n'.join(join_string)
