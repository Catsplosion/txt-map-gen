from copy import copy

from .rect import Rect
from .general import _excl_rand


class RectTree:
    def __init__(self, rect_list=None, max_x=20, max_y=20,
                 x_new_rect_limit=1, y_new_rect_limit=1):
        if rect_list is None:
            self.rects = []
        else:
            self.rects = rect_list

        self.max_x = max_x
        self.max_y = max_y
        self.x_lim = x_new_rect_limit
        self.y_lim = y_new_rect_limit
        self.iters = 0
        # NOTE: Important, here we have to actually keep a rec count
        # instead of just using len(self.rects) everywhere so we can
        # decrement it without actually shortening the list for the
        # recursive calls to split_rect. Otherwise we overflow.

    def __str__(self):
        char_mapping = self.map_list()
        return '\n'.join([' '.join([str(cell) for cell in row])
                          for row in char_mapping])


    def map_list(self):
        mapping = [[0] * self.max_x for i in range(0, self.max_y)]
        char_mapping = [['!'] * self.max_x for i in range(0, self.max_y)]
        for rect in self.rects:
            for y in range(rect.low_y, rect.high_y):
                for x in range(rect.low_x, rect.high_x):
                    mapping[y][x] += 1

        for rect in self.rects:
            for y in range(rect.low_y, rect.high_y):
                for x in range(rect.low_x, rect.high_x):
                    if (y == rect.low_y or y == rect.high_y - 1 or \
                            x == rect.low_x or x == rect.high_x - 1) \
                            and mapping[y][x] > 1:
                        char_mapping[y][x] = '*'
                    elif (y == rect.low_y or y == rect.high_y - 1 or \
                            x == rect.low_x or x == rect.high_x - 1) \
                            and mapping[y][x] == 1:
                        char_mapping[y][x] = '#'
                    else:
                        char_mapping[y][x] = ' '
        return char_mapping

    def __repr__(self):
        class_name = type(self).__name__
        tree_contents = ', '.join([repr(entry) for entry in self.rects])
        return '{}([{}], {}, {}, {}, {})'.format(class_name, tree_contents,
                                                 self.max_x, self.max_y,
                                                 self.x_lim, self.y_lim)


    # TODO: Consider removing the recursive calls in here so we can stop tracking
    # rect count and keeping the list length.
    def split_rects(self, rect1, rect2):
        # TODO: Save all the rect1 information
        # TODO: I dont think i am going to need this copy.
        # we can probably just move the old_rect reference into here
        # and when we remove from the list it simply leaves the list.
        old_rect = copy(rect1)
        self._remove_rect(rect1)
        # TODO: NOTE: Fixed the damn rect system so that now when a rooms
        # rect is bubbled through the collisions, it actually can never have
        # a colliding rect. So the check room is now definitely not needed!
        # YAYYY!! Simply made this counter, and you can see below, if we
        # actually found a collision, which means there is a possibility
        # rects where added and removed, we reset the counter and go through
        # them all again.
        # From measurement we seem to get in the range of the same number
        # of iterations, and no calls to check_room!
        i = 0
        while i < len(self.rects):
            # NOTE: Go back to using len(self.rects) instead of a maintained
            # count and just skip an iteration if our index is out of range
            # if i >= len(self.rects):
            #     continue

            #print("There are: ", len(self.rects), "rects")
            #print("Checking ", i)


            # TODO: Need to get the intersecting rect out of this.
            # How should we do that, reference or 2 passes out.
            intersecting, intersect_rect = self._intersect(self.rects[i], rect2)
            if intersecting:
                # Recursive call with intersect rect
                self.split_rects(self.rects[i], intersect_rect)
                i = 0
                continue
            i += 1
            self.iters += 1 # TODO: Remove this, was just to measure.


        # TODO: We may need to reinstate these settings for the limits
        # as it may interact with the room creation in some way?
        # But for now lets just parameterise the lim directly.
        # if old_rect.high_y < self.max_y - 1:
        #     ly_lim = 2 * YLIM + 4
        # else:
        #     ly_lim = YLIM + 5
        #
        # if old_rect.high_x < self.max_x - 1:
        #     lx_lim = 2 * XLIM + 4
        # else:
        #     lx_lim = XLIM + 5
        #
        # if old_rect.low_y > 0:
        #     hy_lim = 2 * YLIM + 4
        # else:
        #     hy_lim = YLIM + 5
        #
        # if old_rect.low_x > 0:
        #     hx_lim = 2 * XLIM + 4
        # else:
        #     hx_lim = XLIM + 5

        # TODO: Think about how these lims are actually related to room size.
        # Like. Its just the minimum size a rect can be really. So do we
        # make it only spawn a rect if it is at least the minimum size.
        if rect2.low_y - old_rect.low_y - 1 > self.y_lim:
            r = copy(old_rect)
            r.high_y = rect2.low_y - 4
            self.add_rect(r)

        # TODO: Note this 5 here is somehow related to the min space x and y's.
        # If it is under that value it seems to spread the rooms out more, by
        # at least itself - 2. But if it is >= the value it crashes.
        if rect2.low_x - old_rect.low_x - 1 > self.x_lim:
            r = copy(old_rect)
            r.high_x = rect2.low_x - 4
            self.add_rect(r)


        if old_rect.high_y - rect2.high_y - 1 > self.y_lim:
            r = copy(old_rect)
            r.low_y = rect2.high_y + 4
            self.add_rect(r)


        if old_rect.high_x - rect2.high_x - 1 > self.x_lim:
            r = copy(old_rect)
            r.low_x = rect2.high_x + 4
            self.add_rect(r)


    def _intersect(self, rect1, rect2):
        # Initial corner check to see if collision possible.
        if rect2.low_x > rect1.high_x or rect2.low_y > rect1.high_y \
                or rect2.high_x < rect1.low_x or rect2.high_y < rect1.low_y:
            return False, None

        rect3 = Rect()

        # Get the actual possible intersect space.
        if rect2.low_x > rect1.low_x:
            rect3.low_x = rect2.low_x
        else:
            rect3.low_x = rect1.low_x

        if rect2.low_y > rect1.low_y:
            rect3.low_y = rect2.low_y
        else:
            rect3.low_y = rect1.low_y

        if rect2.high_x > rect1.high_x:
            rect3.high_x = rect1.high_x
        else:
            rect3.high_x = rect2.high_x

        if rect2.high_y > rect1.high_y:
            rect3.high_y = rect1.high_y
        else:
            rect3.high_y = rect2.high_y

        # Do the real check if that intersect space was a valid rect.
        if rect3.low_x > rect3.high_x or rect3.low_y > rect3.high_y:
            return False, None
        return True, rect3

    def _remove_rect(self, rect):
        index = self._get_rect_index(rect)
        if index >= 0:
            # NOTE: We went back to using del here due to the check for over
            # len for index in split_rects.
            del self.rects[index]

    def _get_rect_index(self, rect):
        for i in range(0, len(self.rects)):
            if rect.low_x == self.rects[i].low_x \
                    and rect.high_x == self.rects[i].high_x \
                    and rect.low_y == self.rects[i].low_y \
                    and rect.high_y == self.rects[i].high_y:
                return i
        # TODO: Is this how i return this error?
        return -1

    def add_rect(self, rect):
        # Note: Here ignoring the maxrect check that is in the
        # original code because we aren't really interested in setting
        # a maximum number of rects.

        # Check that the rect we want to add isn't included inside one
        # that exists
        if self._rect_contained_in_tree(rect):
            return
        self.rects.append(rect)


    def _rect_contained_in_tree(self, rect):
        for i in range(0, len(self.rects)):
            if rect.low_x >= self.rects[i].low_x \
                    and rect.low_y >= self.rects[i].low_y \
                    and rect.high_x <= self.rects[i].high_x \
                    and rect.high_y <= self.rects[i].high_y:
                return True
        return False

    def rand_rect(self):
        if len(self.rects) > 0:
            return self.rects[_excl_rand(len(self.rects))]
        else:
            return None
