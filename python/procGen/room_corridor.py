from .rect import Rect
from .rect_tree import RectTree
from .general import _excl_rand
from .point_map import PointType, PointMap


class RoomCorridor:
    """ A class for the generation of room and corridor type levels.

    This class allows you to generate room and corridor type levels
    into its data structures for access. It allows the specifying
    of maximum and minumum values for the total space, and the rooms.
    """

    # region Public-interface
    def __init__(self, max_rooms=15, max_x=40, max_y=40,
                 min_space_x=4, min_space_y=4,
                 min_room_dx=3, min_room_dy=3,
                 max_room_dx=5, max_room_dy=5):
        """ Constructor for a RoomCorridor class, initializing all required
        parameters.

        :param max_rooms: The maximum number of rooms allowed to be generated.
        :param max_x: The maximum x dimension of the whole generation space.
        :param max_y: The maximum y dimension of the whole generation space.
        :param min_space_x: The minimum space between rooms on the x axis.
        :param min_space_y: The minimum space between rooms on the axis.
        :param min_room_dx: The minimum x size of a room.
        :param min_room_dy: The minimum y size of a room.
        :param max_room_dx: The maximum x size of a room.
        :param max_room_dy: The maximum y size of a room.

        :ivar self.rooms: A list of rectangles specifying room dimensions, sorted
        ascending on their low x value.
        :ivar self.point_map: A 2D list of PointTypes of the complete level.
        """

        # TODO: I think the min_space values are actually just going to
        #  be the minimum room d values. Because really its the
        # minimum size a rect can be in the tree.
        # So just don't spawn a rect if it is smaller than the min room size?

        full_rect = Rect(2, 2, max_x - 2, max_y - 2)
        self._rect_tree = RectTree([full_rect], max_x=max_x, max_y=max_y,
                                   x_new_rect_limit=min_space_x,
                                   y_new_rect_limit=min_space_y)
        self.rooms = []
        self.max_rooms = max_rooms
        self.max_x = max_x
        self.max_y = max_y
        self.x_limit = min_space_x
        self.y_limit = min_space_y
        # TODO: Maybe cap the min room d vals at 1 and max?
        self.min_room_dx = min_room_dx
        self.min_room_dy = min_room_dy
        self.max_room_dx = max_room_dx
        self.max_room_dy = max_room_dy
        # TODO: Now we have a point map here we need to actually do a "do_rooms"
        # function to place the correct wall types into the map.
        self.point_map = PointMap(max_x, max_y)
        self.corridor_top_lefts = []
        self._joined = []

    def __str__(self):
        # mapping = [['#'] * self.max_y for i in range(0, self.max_x)]
        # for x in range(0, self.max_x):
        #     for y in range(0, self.max_y):
        #         mapping[x][y] = str(self.point_map[x][y])
        # return '\n'.join([' '.join([str(cell) for cell in row])
        #                   for row in mapping])
        return str(self.point_map)

    def __repr__(self):
        class_name = type(self).__name__
        return '{}({}, {}, {}, {}, {}, {}, {}, {}, {})'.format(class_name,
                                                               self.max_rooms,
                                                               self.max_x,
                                                               self.max_y,
                                                               self.x_limit,
                                                               self.y_limit,
                                                               self.min_room_dx,
                                                               self.min_room_dy,
                                                               self.max_room_dx,
                                                               self.max_room_dy)

    def make_rooms(self):
        """ Creates the rooms and corridors, populating the classes accessible
        data structures with the relevant information.

        :return: Returns once complete.
        """
        room_count = 0
        while room_count < self.max_rooms and self._create_a_room():
            room_count += 1
        self._sort_rooms()
        self._map_rooms()
        # TODO: Consider making make_corridors() public interface.
        self._make_corridors()
        return

    def left_justify(self):
        left_most = -1
        for y in range(0, self.max_y):
            for x in range(0, self.max_x):
                point = self.point_map[x, y]
                if point == PointType.CORRIDOR_WALL or \
                        point == PointType.HORIZ_WALL or \
                        point == PointType.VERT_WALL:
                    if x < left_most or left_most == -1:
                        left_most = x

        justified = PointMap(self.max_x - left_most, self.max_y)

        for y in range(0, self.max_y):
            for x in range(0, self.max_x - left_most):
                justified[x, y] = self.point_map[x + left_most, y]

        # TODO: Would be better if this didn't mutate the object, for now though
        # it does!
        self.point_map = justified

        for room in self.rooms:
            room.low_x -= left_most
            room.high_x -= left_most

        for corridor in self.corridor_top_lefts:
            corridor[0] -= left_most


        self.max_x -= left_most

    # endregion

    # region Private-interface
    def _sort_rooms(self):
        # Sort the rooms ascending on low x value.
        self.rooms.sort(key=lambda rect: rect.low_x)

    def _create_a_room(self, x=-1, y=-1, w=-1, h=-1):
        # Creates a room. Currently the only options it will handle are if
        # all provided variables are -1, and generates a random room.

        # TODO: Extend to allow placement of predefined rooms.

        tries = 0
        finish = False

        while tries < 100 and not finish:
            # This if when all args are random.
            if x < 0 and y < 0 and w < 0 and h < 0:
                rect1 = self._rect_tree.rand_rect()

                if rect1 is None:
                    # No random rects left.
                    return False

                # NOTE: This is an improvement to the original, and not
                # how the original works.
                # Generate the rooms coordinates within the supplied
                # rectangles coordinates.
                rect_dx = rect1.high_x - rect1.low_x
                rect_dy = rect1.high_y - rect1.low_y

                if rect_dx < self.min_room_dx:
                    dx_variance_min = 1
                    dx_variance_max = rect_dx - dx_variance_min
                else:
                    dx_variance_max = self.max_room_dx
                    dx_variance_min = self.min_room_dx


                if rect_dy < self.min_room_dy:
                    dy_variance_min = 1
                    dy_variance_max = rect_dy - dy_variance_min

                else:
                    dy_variance_max = self.max_room_dy
                    dy_variance_min = self.min_room_dy

                dx = self.min_room_dx + _excl_rand(dx_variance_max - dx_variance_min)
                dy = self.min_room_dy + _excl_rand(dy_variance_max - dy_variance_min)


                # TODO: This border seems to be some limit that the room needs
                # between its boundaries and that of it's enclosing rectangle.
                # Must evaluate whether we actually need it, i suspect not.
                # Leave it for now.
                if rect1.low_x > 0 and rect1.high_x < self.max_x - 1:
                    x_border = 2 * self.x_limit
                else:
                    x_border = self.x_limit + 1

                if rect1.low_y > 0 and rect1.high_y < self.max_y - 1:
                    y_border = 2 * self.y_limit
                else:
                    y_border = self.y_limit + 1

                # Don't fit with the border added on.
                if rect1.high_x - rect1.low_x < dx + x_border \
                        or rect1.high_y - rect1.low_y < dy + y_border:
                    finish = False
                    tries += 1
                    continue

                if rect1.low_x > 0:
                    val1 = self.x_limit
                    val2 = rect1.low_x
                else:
                    val1 = 3
                    val2 = 3

                # Find the hard x point at which to place the room within
                # the rectangle. So its from the rectangles low x, plus
                # some random dx within the rectangle, minus the dx of
                # the room so we have enough space to fit the room.
                absolute_x = rect1.low_x + _excl_rand(rect1.high_x -
                                                      rect1.low_x - dx)

                # Same as above for the y absolute.
                absolute_y = rect1.low_y + _excl_rand(rect1.high_y -
                                                      rect1.low_y - dy)


                # TODO: This check is very confusing. Like what the hell is going on.
                # This game specific implementation was removed.

                # if rect1.low_y == 0 and rect1.high_y >= (self.max_y - 1) \
                #         and (not len(self.rooms) or not _excl_rand(len(self.rooms))) \
                #         and (absolute_y + dy > self.max_y // 2):
                #     absolute_y = _excl_rand(2) + 2
                #
                #     if len(self.rooms) < 4 and dy > 1:
                #         dy -= 1

                # A rectangle representing the room.
                rect2 = Rect(absolute_x, absolute_y,
                             absolute_x + dx, absolute_y + dy)

                finish = True

            # TODO: Here is all the similar stuff for when some values are specified
            # For now just do random and come back to this.

        # No room created
        if not finish:
            return False

        self._rect_tree.split_rects(rect1, rect2)

        self._add_room(rect2)
        return True

    def _add_room(self, room):
        # Add a room to the relevant structures.

        # NOTE: Put the addition to _joined first so that the first room added
        # goes in as 0? Not sure if we want this or go in as 1?
        self._joined.append(len(self.rooms))
        self.rooms.append(room)
        # if room.low_x < self.left_justify or self.left_justify == -1:
        #     self.left_justify = room.low_x

    def _map_rooms(self):
        # Map the rooms from their coordinates onto the point map array.
        # This MUST be done before corridor generation can be done.

        # TODO: in Nethack There is a check here for bumping against the edge
        # and then adding or subtracting to move the room away from the edge, i
        # don't thin this matter to us at all.

        for room in self.rooms:
            for x in range(room.low_x, room.high_x + 1):
                for y in range(room.low_y, room.high_y + 1):
                    if x == room.low_x or x == room.high_x:
                        self.point_map[x, y] = PointType.VERT_WALL
                    elif y == room.low_y or y == room.high_y:
                        self.point_map[x, y] = PointType.HORIZ_WALL
                    else:
                        self.point_map[x, y] = PointType.ROOM

            self.point_map[room.low_x, room.low_y] = PointType.TL_CORNER
            self.point_map[room.high_x, room.low_y] = PointType.TR_CORNER
            self.point_map[room.low_x, room.high_y] = PointType.BL_CORNER
            self.point_map[room.high_x, room.high_y] = PointType.BR_CORNER

    def _make_corridors(self):
        # Make the corridors between rooms.

        # Corridors left to right.
        for i in range(0, len(self.rooms) - 1):
            self._join(i, i + 1, False)
            if not _excl_rand(50):
                break

        # corridors left to right skip 2 if not joined.
        for i in range(0, len(self.rooms) - 2):
            # Here joined stores which rooms are joined to which, by index to
            # value.
            if self._joined[i] != self._joined[i + 2]:
                self._join(i, i + 2, False)

        # Complete joining so all rooms are joined to chain.
        available = True
        for i in range(0, len(self.rooms)):
            if not available:
                break
            available = False
            for j in range(0, len(self.rooms)):
                if self._joined[i] != self._joined[j]:
                    self._join(i, j, False)
                    available = True

        # TODO: Random joining is currently turned off because it would mess
        # with chisel use because merged corridors would have the same index
        # Make this an option for configurability.

        # Random joining.
        # if len(self.rooms) > 2:
        #     for i in range(_excl_rand(len(self.rooms) + 4), 0, -1):
        #         room_index_1 = _excl_rand(len(self.rooms))
        #         room_index_2 = _excl_rand(len(self.rooms) - 2)
        #
        #         if room_index_2 >= room_index_1:
        #             room_index_2 += 2
        #
        #         self._join(room_index_1, room_index_2, True)

    def _join(self, room_1_index, room_2_index, nxcor):
        # This function will try to join the two specified rooms
        # with a corridor. nxcor can be used to specify whether this
        # is not a vital corridor placement. If it is False some restrictions
        # are ignored.

        # TODO: If we introduce DOORMAX possibly do a check for it here like
        # original code.

        room_1 = self.rooms[room_1_index]
        room_2 = self.rooms[room_2_index]

        if room_2.low_x > room_1.high_x:
            dx = 1
            dy = 0

            x1 = room_1.high_x
            x2 = room_2.low_x

            # For x1
            coord_1 = self._find_door_pos(x1, room_1.low_y, x1, room_1.high_y)
            # For x2
            coord_2 = self._find_door_pos(x2, room_2.low_y, x2, room_2.high_y)

        elif room_2.high_y < room_1.low_y:
            dx = 0
            dy = -1

            y1 = room_1.low_y
            y2 = room_2.high_y

            # For yy
            coord_1 = self._find_door_pos(room_1.low_x, y1, room_1.high_x, y1)
            # For ty
            coord_2 = self._find_door_pos(room_2.low_x, y2, room_2.high_x, y2)

        elif room_2.high_x < room_1.low_x:
            dx = -1
            dy = 0

            x1 = room_1.low_x
            x2 = room_2.high_x

            # For xx
            coord_1 = self._find_door_pos(x1, room_1.low_y, x1, room_1.high_y)
            # For tx
            coord_2 = self._find_door_pos(x2, room_1.low_y, x2, room_1.high_y)

        else:
            dx = 0
            dy = 1

            y1 = room_1.high_y
            y2 = room_2.low_y

            # For yy
            coord_1 = self._find_door_pos(room_1.low_x, y1, room_1.high_x, y1)
            # For ty
            coord_2 = self._find_door_pos(room_2.low_x, y2, room_2.high_x, y2)


        if nxcor and self.point_map[coord_1[0] + dx, coord_1[1] + dy] \
                != PointType.STONE:
            return

        # If nxcor is not set then we are doing possibly essential room
        # connection so even if we dont have a good position for a door
        # slam one down anywat.
        if self._ok_door(coord_1[0], coord_1[1]) or not nxcor:
            self._do_door(coord_1)

        # In front of there respective doors.
        origin = [coord_1[0] + dx, coord_1[1] + dy]
        destination = [coord_2[0] - dx, coord_2[1] - dy]

        if not self._alternate_dig_corridor(origin, destination, dx, dy, nxcor):
            return

        # Successful dig so place second door.
        if self._ok_door(coord_2[0], coord_2[1]) or not nxcor:
            self._do_door(coord_2)

        # Assign the rooms indices to each other in the joined list so we
        # know they are joined for future connections.
        if self._joined[room_1_index] < self._joined[room_2_index]:
            self._joined[room_2_index] = self._joined[room_1_index]
        else:
            self._joined[room_1_index] = self._joined[room_2_index]

    def _do_door(self, coord):
        # Places a door in the point map.
        self.point_map[coord[0], coord[1]] = PointType.DOOR

    def _dig_corridor(self, origin, destination, nxcor):
        # This is our port of the original NetHack method for digging
        # the dungeons corridors. It will dig a 1x1 corridor
        # between the rooms..

        # TODO: Putting the out of bounds check anyway, not really sure
        # we need it.
        if not 0 <= origin[0] < (self.max_x - 1) or not \
                0 <= origin[1] < (self.max_y - 1) or not \
                0 <= destination[0] < (self.max_x - 1) or not \
                0 <= destination[1] < (self.max_y - 1):
            return False

        dx = dy = 0

        # Set digging direction
        if destination[0] > origin[0]:
            dx = 1
        elif destination[1] > origin[1]:
            dy = 1
        elif destination[0] < origin[0]:
            dx = -1
        else:
            dy = -1

        digging_attempts = 0
        while origin[0] != destination[0] or origin[1] != destination[1]:
            digging_attempts += 1

            # TODO: Arbitrary digging attempts limit, look into this!
            # TODO: Arbitrary 1 out of 35 exit chance.
            if digging_attempts > 500 or (nxcor and not _excl_rand(35)):
                return False

            # TODO: Maybe remove this bounds check. As the NetHack code states
            # this condition should be impossible because the target should
            # be out of bounds.
            if not 0 <= origin[0] <= (self.max_x - 1) or not \
                    0 <= origin[1] < (self.max_y - 1):
                return False

            if self.point_map[origin[0], origin[1]] == PointType.STONE:
                # TODO: At this point in NetHacks code they have a 1 in 100
                # chance of not placing a corridor, but a secret corridor instead.
                # we are just going to place a corridor. Need to ensure this has
                # No impact on the failure chance and so the result.
                self.point_map[origin[0], origin[1]] = PointType.CORRIDOR
            else:
                # Here we have hit something we weren't expecting!
                return False

            # find the next move position.
            dix = abs(origin[0] - destination[0])
            diy = abs(origin[1] - destination[1])

            # Random inefficiencies.
            if (dix > diy) and diy and not _excl_rand(dix - diy + 1):
                dix = 0
            elif (diy > dix) and dix and not _excl_rand(diy - dix + 1):
                diy = 0

            # This is a change of direction to the x axis from the y
            if dy and (dix > diy):
                if origin[0] > destination[0]:
                    ddx = -1
                else:
                    ddx = 1

                point = self.point_map[origin[0] + ddx, origin[1]]
                if point == PointType.STONE or point == PointType.CORRIDOR:
                    dx = ddx
                    dy = 0
                    continue

            # And this is a change in direction to the y axis from the x
            elif dx and (diy > dix):
                if origin[1] > destination[1]:
                    ddy = -1
                else:
                    ddy = 1

                point = self.point_map[origin[0], origin[1] + ddy]
                if point == PointType.STONE or point == PointType.CORRIDOR:
                    dy = ddy
                    dx = 0
                    continue

            # So now we are here, we aren't changing direction, so continue
            # straight on
            point = self.point_map[origin[0] + dx, origin[1] + dy]
            if point == PointType.STONE or point == PointType.CORRIDOR:
                continue

            # Okay, the next space is not corridor and Stone so what do now?
            # So if we hit something going on the x axis, switch to y to avoid.
            if dx:
                dx = 0
                if destination[1] < origin[1]:
                    dy = -1
                else:
                    dy = 1
            # We where traveling on the y, so now go x to avoid.
            else:
                dy = 0
                if destination[0] < origin[0]:
                    dx = -1
                else:
                    dx = 1
            point = self.point_map[origin[0] + dx, origin[1] + dy]
            if point == PointType.STONE or point == PointType.CORRIDOR:
                continue

            # Okay that direction wasn't good either, so flip the dx and dy and
            # go back to try
            dy = -dy
            dx = -dx

        # Tunnel got dug
        return True

    def _alternate_dig_corridor(self, origin, destination, clearnance_x, clearnance_y, nxcor):
        # Digs a 3x1 corridor as opposed to a 1x1 in the original code.

        # TODO: do we need this out of bounds check?
        if not 0 <= origin[0] < (self.max_x - 1) or not \
                0 <= origin[1] < (self.max_y - 1) or not \
                0 <= destination[0] < (self.max_x - 1) or not \
                0 <= destination[1] < (self.max_y - 1):
            return False

        # Before the main algorithm do clearance to facilitate 3*1.
        for i in range(0, 2):
            if clearnance_x:
                front = [[origin[0], origin[1] - 1],
                         origin,
                         [origin[0], origin[1] + 1]]
            else:
                front = [[origin[0] - 1, origin[1]],
                         origin,
                         [origin[0] + 1, origin[1]]]

            # Check if we hit something unexpected on the entire front.
            for point in front:
                if not (self.point_map[point] == PointType.STONE or
                        self.point_map[point] == PointType.CORRIDOR_WALL or
                        self.point_map[point] == PointType.CORRIDOR):
                    return False

            for point in front:
                self.point_map[point] = PointType.CORRIDOR

            # Create the walls around the front if it is stone.
            walls = []
            # Select walls if we are travelling across x.
            if clearnance_x:
                for j in range(-2, 3):
                    if clearnance_x > 0:
                        point = [origin[0] + 1, origin[1] + j]
                    else:
                        point = [origin[0] - 1, origin[1] + j]

                    if self.point_map[point] == PointType.STONE:
                        walls.append(point)

                for j in range(-1, 1):
                    point = [origin[0] + j, origin[1] + 2]
                    if self.point_map[point] == PointType.STONE:
                        walls.append(point)
                    point = [origin[0] + j, origin[1] - 2]
                    if self.point_map[point] == PointType.STONE:
                        walls.append(point)

            # Select walls if we are travelling across y.
            if clearnance_y:
                for k in range(-2, 3):
                    if clearnance_y > 0:
                        point = [origin[0] + k, origin[1] + 1]
                    else:
                        point = [origin[0] + k, origin[1] - 1]

                    if self.point_map[point] == PointType.STONE:
                        walls.append(point)

                for k in range(-1, 1):
                    point = [origin[0] + 2, origin[1] + k]
                    if self.point_map[point] == PointType.STONE:
                        walls.append(point)
                    point = [origin[0] - 2, origin[1] + k]
                    if self.point_map[point] == PointType.STONE:
                        walls.append(point)

            # Fill out selected walls.
            for wall in walls:
                self.point_map[wall] = PointType.CORRIDOR_WALL

            # We only want to advance after first clearance, no the second
            # so if a direction change occures immediately in the rest of the
            # algorithm, it wont dig a 1x1 spot.
            if i == 0:
                origin = [origin[0] + clearnance_x, origin[1] + clearnance_y]

        # Fill out the top left variable for public access.
        top_left = front[0]
        self.corridor_top_lefts.append(top_left)

        dx = dy = 0

        # Set digging direction
        if destination[0] > origin[0]:
            dx = 1
        elif destination[1] > origin[1]:
            dy = 1
        elif destination[0] < origin[0]:
            dx = -1
        else:
            dy = -1

        # Build applicable front.
        if dx:
            front = [[origin[0], origin[1] - 1],
                     origin,
                     [origin[0], origin[1] + 1]]
        else:
            front = [[origin[0] - 1, origin[1]],
                     origin,
                     [origin[0] + 1, origin[1]]]

        # Check if we hit something unexpected on the entire front.
        for point in front:
            if not (self.point_map[point] == PointType.STONE or
                    self.point_map[point] == PointType.CORRIDOR_WALL or
                    self.point_map[point] == PointType.CORRIDOR):
                return False

        digging_attempts = 0
        while origin[0] != destination[0] or origin[1] != destination[1]:
            digging_attempts += 1

            # TODO: Arbitrary digging attempts limit, look into this!
            # TODO: Arbitrary 1 out of 35 exit chance.
            if digging_attempts > 500 or (nxcor and not _excl_rand(35)):
                return False

            # TODO: Maybe remove this bounds check. As the NetHack code states
            # this condition should be impossible. Rooms cant be that close to
            # the edge that the target could be outside it.
            if not 0 <= origin[0] <= (self.max_x - 1) or not \
                    0 <= origin[1] < (self.max_y - 1):
                return False

            # Check if we hit something unexpected on the entire front.
            for point in front:
                if not (self.point_map[point] == PointType.STONE or
                        self.point_map[point] == PointType.CORRIDOR_WALL or
                        self.point_map[point] == PointType.CORRIDOR):
                    return False

            # Did not hit anything unexpected so set the entire front to corridor.
            for point in front:
                self.point_map[point] = PointType.CORRIDOR

            # Create the walls around the front if it is stone.
            walls = []
            # Select walls if we are travelling across x.
            if dx:
                for i in range(-2, 3):
                    if dx > 0:
                        point = [origin[0] + 1, origin[1] + i]
                    else:
                        point = [origin[0] - 1, origin[1] + i]

                    if self.point_map[point] == PointType.STONE:
                        walls.append(point)

                for i in range(-1, 1):
                    point = [origin[0] + i, origin[1] + 2]
                    if self.point_map[point] == PointType.STONE:
                        walls.append(point)
                    point = [origin[0] + i, origin[1] - 2]
                    if self.point_map[point] == PointType.STONE:
                        walls.append(point)

            # Select walls if we are travelling across y.
            if dy:
                for i in range(-2, 3):
                    if dy > 0:
                        point = [origin[0] + i, origin[1] + 1]
                    else:
                        point = [origin[0] + i, origin[1] - 1]

                    if self.point_map[point] == PointType.STONE:
                        walls.append(point)

                for i in range(-1, 1):
                    point = [origin[0] + 2, origin[1] + i]
                    if self.point_map[point] == PointType.STONE:
                        walls.append(point)
                    point = [origin[0] - 2, origin[1] + i]
                    if self.point_map[point] == PointType.STONE:
                        walls.append(point)

            # Fill out selected walls.
            for wall in walls:
                self.point_map[wall] = PointType.CORRIDOR_WALL

            # find the next move position.
            dix = abs(origin[0] - destination[0])
            diy = abs(origin[1] - destination[1])

            # TODO: Removed random chance of not tavelling in the right direction
            # Put back if we want it.

            # if (dix > diy) and diy and not _excl_rand(dix - diy + 1):
            #     dix = 0  # No x movement
            # elif (diy > dix) and dix and not _excl_rand(diy - dix + 1):
            #     diy = 0  # No y movement

            # This is a change of direction to the x axis from the y.
            if dy and (dix > diy):
                if origin[0] > destination[0]:
                    ddx = -1
                else:
                    ddx = 1

                # Set the front for the avoidance check.
                front = [[origin[0] + ddx, origin[1] - 1],
                         [origin[0] + ddx, origin[1]],
                         [origin[0] + ddx, origin[1] + 1]]

                # Do the avoidance check.
                carry_on = True
                for front_point in front:
                    point = self.point_map[front_point]
                    if not (point == PointType.STONE or point == PointType.CORRIDOR or
                            point == PointType.CORRIDOR_WALL):
                        carry_on = False

                if carry_on:
                    origin = front[1]
                    dx = ddx
                    dy = 0
                    continue

            # And this is a change in direction to the y axis from the x.
            elif dx and (diy > dix):
                if origin[1] > destination[1]:
                    ddy = -1
                else:
                    ddy = 1

                # Set the front for the avoidance check.
                front = [[origin[0] - 1, origin[1] + ddy],
                         [origin[0], origin[1] + ddy],
                         [origin[0] + 1, origin[1] + ddy]]

                carry_on = True
                for front_point in front:
                    point = self.point_map[front_point]
                    if not (point == PointType.STONE or point == PointType.CORRIDOR or
                            point == PointType.CORRIDOR_WALL):
                        carry_on = False

                if carry_on:
                    origin = front[1]
                    dy = ddy
                    dx = 0
                    continue

            # So now we are here, we aren't changing direction, so continue
            # straight on.
            # Set the front for the avoidance check.
            if dx:
                front = [[origin[0] + dx, origin[1] - 1],
                         [origin[0] + dx, origin[1]],
                         [origin[0] + dx, origin[1] + 1]]
            else:
                front = [[origin[0] - 1, origin[1] + dy],
                         [origin[0], origin[1] + dy],
                         [origin[0] + 1, origin[1] + dy]]

            carry_on = True
            for front_point in front:
                point = self.point_map[front_point]
                if not (point == PointType.STONE or point == PointType.CORRIDOR or
                        point == PointType.CORRIDOR_WALL):
                    carry_on = False

            if carry_on:
                origin = front[1]
                continue

            # Okay, the next space is not corridor and Stone so what do now?
            # So if we hit something going on the x axis, switch to y to avoid.
            if dx:
                # Shift back one to facilitate 3*3s
                origin = [origin[0] - dx, origin[1]]
                dx = 0
                if destination[1] <= origin[1]:
                    dy = -1
                else:
                    dy = 1

            # We where traveling on the y, so now go x to avoid.
            else:
                # Shift back one to facilitate 3*3
                origin = [origin[0], origin[1] - dy]
                dy = 0
                if destination[0] <= origin[0]:
                    dx = -1
                else:
                    dx = 1

            # Set the front for the avoidance check
            if dx:
                front = [[origin[0] + dx, origin[1] - 1],
                         [origin[0] + dx, origin[1]],
                         [origin[0] + dx, origin[1] + 1]]
            else:
                front = [[origin[0] - 1, origin[1] + dy],
                         [origin[0], origin[1] + dy],
                         [origin[0] + 1, origin[1] + dy]]

            carry_on = True
            for front_point in front:
                point = self.point_map[front_point]
                if not (point == PointType.STONE or point == PointType.CORRIDOR or
                        point == PointType.CORRIDOR_WALL):
                    carry_on = False

            if carry_on:
                origin = front[1]
                continue

            # Okay that direction wasn't good either, so flip the dx and dy and
            # go back to try
            dy = -dy
            dx = -dx

        # Go through the last and final front and dig it out.
        for point in front:
            self.point_map[point] = PointType.CORRIDOR

        walls = []
        # Select walls if we are travelling across x.
        if dx:
            for i in range(-2, 3):
                if dx > 0:
                    point = [origin[0] + 1, origin[1] + i]
                else:
                    point = [origin[0] - 1, origin[1] + i]

                if self.point_map[point] == PointType.STONE:
                    walls.append(point)

            for i in range(-1, 1):
                point = [origin[0] + i, origin[1] + 2]
                if self.point_map[point] == PointType.STONE:
                    walls.append(point)
                point = [origin[0] + i, origin[1] - 2]
                if self.point_map[point] == PointType.STONE:
                    walls.append(point)

        # Select walls if we are travelling across y.
        if dy:
            for i in range(-2, 3):
                if dy > 0:
                    point = [origin[0] + i, origin[1] + 1]
                else:
                    point = [origin[0] + i, origin[1] - 1]

                if self.point_map[point] == PointType.STONE:
                    walls.append(point)

            for i in range(-1, 1):
                point = [origin[0] + 2, origin[1] + i]
                if self.point_map[point] == PointType.STONE:
                    walls.append(point)
                point = [origin[0] - 2, origin[1] + i]
                if self.point_map[point] == PointType.STONE:
                    walls.append(point)

        for wall in walls:
            self.point_map[wall] = PointType.CORRIDOR_WALL

        # Tunnel got dug
        return True

    def _find_door_pos(self, low_x, low_y, high_x, high_y):
        # Finds a safe position for a door within the rectangle specified.

        # The plus 1 to keep from passing - to _excl_rand.
        x = _excl_rand(high_x - low_x + 1) + low_x
        y = _excl_rand(high_y - low_y + 1) + low_y

        if self._ok_door(x, y):
            return [x, y]

        for x in range(low_x, high_x + 1):
            for y in range(low_y, high_y + 1):
                # self.point_map[x][y] = PointType.QUESTION
                # print(self)
                if self._ok_door(x, y):
                    return [x, y]

        for x in range(low_x, high_x + 1):
            for y in range(low_y, high_y + 1):
                # self.point_map[x][y] = PointType.QUESTION
                # print(self)
                if self.point_map[x, y] == PointType.DOOR:
                    return [x, y]

        # No reasonable spot??
        # return something.
        return [low_x, high_y]

    def _ok_door(self, x, y):
        # Check to see if this spot is a good spot for a door.

        # if we decide we need DOORMAX as the original
        # algorithm hasit should be added here.
        return (self.point_map[x, y] == PointType.HORIZ_WALL or
                self.point_map[x, y] == PointType.VERT_WALL) and not \
                self._near_door(x, y)

    def _near_door(self, x, y):
        # Check to see iif are within one space in any cardinal direction of
        # a door tile.
        if self._in_bounds(x + 1, y):
            if self.point_map[x + 1, y] == PointType.DOOR:
                return True

        if self._in_bounds(x - 1, y):
            if self.point_map[x - 1, y] == PointType.DOOR:
                return True

        if self._in_bounds(x, y + 1):
            if self.point_map[x, y + 1] == PointType.DOOR:
                return True

        if self._in_bounds(x, y - 1):
            if self.point_map[x, y - 1] == PointType.DOOR:
                return True

    def _in_bounds(self, x, y):
        # Check if these x,y coordinates are in our bounds.
        return 0 <= x < self.max_x and 0 <= y < self.max_y

    # endregion

    # region Debug

    def _print_rooms(self):
        # Print a room map.
        mapping = [['!'] * self.max_x for i in range(0, self.max_y)]
        for room in self.rooms:
            for y in range(room.low_y, room.high_y + 1):
                for x in range(room.low_x, room.high_x + 1):
                    if y == room.low_y or y == room.high_y:
                        if mapping[y][x] == '#' or mapping[y][x] == ' ':
                            mapping[y][x] = '*'
                        else:
                            mapping[y][x] = '#'
                    elif x == room.low_x or x == room.high_x:
                        if mapping[y][x] == '#' or mapping[y][x] == ' ':
                            mapping[y][x] = '*'
                        else:
                            mapping[y][x] = '#'
                    else:
                        mapping[y][x] = ' '
        print('\n'.join([' '.join([str(cell) for cell in row])
                          for row in mapping]))

    def _print_rooms_and_rect(self):
        # Print the rooms and rectangles in one.
        rect_map = self._rect_tree.map_list()

        for room in self.rooms:
            for y in range(room.low_y, room.high_y):
                for x in range(room.low_x, room.high_x):
                    if rect_map[y][x] != '!':
                        rect_map[y][x] = 'c'
                    else:
                        rect_map[y][x] = 'r'
        print('\n'.join([' '.join([str(cell) for cell in row])
                        for row in rect_map]))

    # endregion
