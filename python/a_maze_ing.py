#!/usr/bin/env python3

import argparse

from procGen.maze import Maze
from procGen.room_corridor import RoomCorridor
from chisel_file import *

#region arparse
parser = argparse.ArgumentParser(
    description='Command line tool to procedurally generate maze, or room and '
                'corridor type level text files.'
)

parser.add_argument('--version', '-v', action='version', version='%(prog)s 1.0.0')

parser.add_argument(
    'mode',
    help='The mode of generation: '
         'Maze type generation or room and corridor type generation.',
    choices=['maze', 'rooms'],
    default='maze'
)


parser.add_argument('--max-x', '-x',
                    help='The x dimension of the generated level if no '
                    'scaling is required.',
                    type=int,
                    default=60)

parser.add_argument('--max-y', '-y',
                    help='The y dimension of the generated level if no '
                    'scaling is required.',
                    type=int,
                    default=60)

parser.add_argument('--output-file', '-out',
                    help='The file to write the level to.',
                    default='generated.lev')


maze_group = parser.add_argument_group(
    'Maze generation arguments',
    'Arguments that should be used with the maze option. '
    'Any more than a value of 1 for both the --corridor-width and '
    '--wall-width arguments will cause scaling of the maze and it '
    'will no longer reach max-x, max-y in size.'
)

maze_group.add_argument('--corridor-width', '-corr',
                        help='The width of the mazes corridors.',
                        type=int,
                        default=1)

maze_group.add_argument('--wall-width', '-wall',
                        help='The width of the mazes walls.',
                        type=int,
                        default=1)

room_group = parser.add_argument_group(
    'Room and corridor generation arguments',
    'Arguments that should be used with the rooms option.'
)

room_group.add_argument('--max-rooms',
                        help='The maximum number of rooms that will be '
                             'generated in the given space.',
                        type=int,
                        default=8)

room_group.add_argument('--min-room-x',
                        help='The minimum possible x dimension of '
                             'generated rooms.',
                        type=int,
                        default=7)

room_group.add_argument('--min-room-y',
                        help='The minimum possible y dimension of '
                             'generated rooms.',
                        type=int,
                        default=7
                        )

room_group.add_argument('--max-room-x',
                        help='The maximum possible x dimension of '
                             'generated rooms.',
                        type=int,
                        default=13
                        )

room_group.add_argument('--max-room-y',
                        help='The maximum possible y dimension of '
                             'generated rooms.',
                        type=int,
                        default=13
                        )
#endregion argparse

arguments = parser.parse_args()

if arguments.mode == 'maze':
    generator = Maze(arguments.max_x, arguments.max_y,
                     arguments.corridor_width,
                     arguments.wall_width)

    generator.generate_maze()
    write_maze(generator, arguments.output_file)
    print('Maze level written to: ', arguments.output_file)

elif arguments.mode == 'rooms':
    generator = RoomCorridor(arguments.max_rooms,
                             arguments.max_x, arguments.max_y,
                             min_space_x=5, min_space_y=5,
                             min_room_dx=arguments.min_room_x,
                             max_room_dx=arguments.max_room_x,
                             min_room_dy=arguments.min_room_y,
                             max_room_dy=arguments.max_room_y)

    generator.make_rooms()
    write_room_corridor(generator, arguments.output_file)
    print('Room and corridor level written to: ', arguments.output_file)
